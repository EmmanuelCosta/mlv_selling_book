#### CHAINE DE LANCEMENT Provisoire
=====================================
0. Lancer le remiregistry dans le repertoire BookStoreService/src
1. Lancer le main de BookStoreService
Note : on peut les modifier pour ajouter un nouveau BookStore
2. Lancer RMI client de BibliothekManager : Cette étape est utile pour peupler les BookStore
Il faut donc lancer le main : MainClient de BibliothekManager
3. Modifier le fichier resource/bookstoreList :
Ce fichier contient la liste des rmi addresse des bookStore, les adresses Ip doivent donc correspondre a ceux des machines surlesquelles vous avez deploye les rmiregistry(et donc les serveurs)
Format : une adresse par ligne
4. Generer le wsdl sur BibliothekManager(web service)
5. l'utiliser pour creer un web service client







