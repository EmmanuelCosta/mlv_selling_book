import java.rmi.RemoteException;
import NET.webserviceX.www.Currency;
import NET.webserviceX.www.CurrencyConvertorSoapProxy;

public class Converter {
	
	
	public double rate(String currency, String currency_dest) throws RemoteException 
	{
		return new CurrencyConvertorSoapProxy().conversionRate(Currency.fromString(currency),Currency.fromString(currency_dest));
	}
	
	public double convert(double val,String currency, String currency_dest) throws RemoteException
	{
		return val*rate(currency,currency_dest);
	}

}
