package fr.umlv.bibliothek.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import fr.umlv.bibliothek.server.BookStore;

public class BookStoreFromFile {
	private final List<BookStore> bookStores = new ArrayList<>();
	private final BufferedReader bookStoreFile;

	public BookStoreFromFile() throws FileNotFoundException {
		String file = getClass().getClassLoader().getResource("bookStoreList")
				.getFile();
		this.bookStoreFile = new BufferedReader(new FileReader(new File(file)));
	}

	public int size() {
		return bookStores.size();
	}

	public List<BookStore> getBookStores() throws IOException {
		for (String bookStoreURL = bookStoreFile.readLine(); bookStoreURL != null; bookStoreURL = bookStoreFile
				.readLine()) {
			System.out.println("===> " + bookStoreURL);

			try {

				BookStore bookStore = (BookStore) Naming.lookup(bookStoreURL);
				bookStores.add(bookStore);
				System.out.println("===> added");
			} catch (NotBoundException | MalformedURLException
					| RemoteException e) {
				System.out.println(bookStoreURL + " not reachable");
			}

		}

		return bookStores;
	}

	public static void main(String[] args) throws IOException {
		BookStoreFromFile bookStoreFromFile = new BookStoreFromFile();

		for (BookStore bookStore : bookStoreFromFile.getBookStores()) {
			for(fr.umlv.bibliothek.server.Book b :bookStore.getAllBooks()){
				long isbn  = b.getISBN();
				System.out.println("book isbn = "+isbn+" category = "+b.getCategory());
				System.out.println(bookStore.getNumberOfAvailableBook(isbn));
				
			}
		}
	}
}
