package fr.umlv.bibliothek.service;

import java.rmi.RemoteException;

public class Book {
	private fr.umlv.bibliothek.server.Book internalBook;

	public Book() {
	}

	public Book(fr.umlv.bibliothek.server.Book book) {
		this.internalBook = book;
	}

	public String getTitle() throws RemoteException {
		return internalBook.getTitle();
	}

	public long getISBN() throws RemoteException {
		return this.internalBook.getISBN();
	}

	public String getAutor() throws RemoteException {
		return this.internalBook.getAutor();
	}

	public String getDescription() throws RemoteException {
		return this.internalBook.getDescription();
	}

	public double getPrice() throws RemoteException {
		return this.internalBook.getPrice();
	}

	public String getCategory() throws RemoteException {
		return this.internalBook.getCategory();
	}

//	public String[] getKeyWords() {
//		return this.internalBook.get
//	}

	public String remoteToString() throws RemoteException {
		return this.internalBook.remoteToString();
	}

}
