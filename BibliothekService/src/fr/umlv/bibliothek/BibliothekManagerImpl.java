package fr.umlv.bibliothek;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import fr.umlv.bibliothek.server.BookStore;
import fr.umlv.bibliothek.service.Book;
import fr.umlv.bibliothek.service.BookStoreFromFile;

public class BibliothekManagerImpl implements BibliothekManager {

	private Map<String, BookStore> bookStorageAdress = new HashMap<String, BookStore>();

	public BibliothekManagerImpl() throws  IOException {
		BookStoreFromFile bookStoreFromFile = new BookStoreFromFile();

		for (BookStore bookStore : bookStoreFromFile.getBookStores()) {
			bookStorageAdress.put(bookStore.getCategory(), bookStore);
		}
	}

	@Override
	public void addSpecificNumberOfBookInstance(Long isbn, String author,
			String title, String description, String category, double price,
			int numberToAdd, String... keywords) throws RemoteException {
		BookStore bookStore = bookStorageAdress.get(category);
		if (Objects.isNull(bookStore)) {
			throw new IllegalArgumentException(
					"there is no storage associate with that Book Category");
		} else {
			bookStore.createBook(isbn, author, title, description, price,
					numberToAdd, keywords);
		}
	}

	@Override
	public void addBook(Long isbn, String author, String title,
			String description, String category, double price,
			String... keywords) throws RemoteException {

		addSpecificNumberOfBookInstance(isbn, author, title, description,
				category, price, 1, keywords);

	}

	@Override
	public Book removeSpecificNumberOfBookInstanceByISBN(long isbn,
			int numberToRemove) throws RemoteException {
		BookStore bookStore = getBookStoreByISBN(isbn);
		if (Objects.isNull(bookStore)) {
			throw new IllegalArgumentException(
					"there is no storage associate with that Book Category");
		} else {
			return new Book(bookStore.removeBookByISBN(isbn, numberToRemove));
		}
	}

	@Override
	public Book removeBookByISBN(long isbn) throws RemoteException {
		return removeSpecificNumberOfBookInstanceByISBN(isbn, 1);
	}

	//
	@Override
	public Book getBookByISBN(long isbn) throws RemoteException {
		BookStore bookStore = getBookStoreByISBN(isbn);

		if (Objects.isNull(bookStore)) {
			throw new IllegalArgumentException(
					"there is no storage associate with that Book Category");
		} else {
			return new Book(bookStore.getBookByISBN(isbn));
		}
	}

	@Override
	public Book[] searchBookByAuthor(String author) throws RemoteException {

		List<fr.umlv.bibliothek.server.Book> books = new ArrayList<>();
		for (BookStore bookStore : bookStorageAdress.values()) {
			List<fr.umlv.bibliothek.server.Book> bookByTitle = bookStore
					.searchBookByAuthor(author);
			if (!bookByTitle.isEmpty()) {
				books.addAll(bookByTitle);
			}
		}
		List<Book> localBooks = getServiceBookListFromRMIBookList(books);
		return localBooks.toArray(new Book[localBooks.size()]);
	}

	@Override
	public Book[] searchBookByTitle(String title) throws RemoteException {
		List<fr.umlv.bibliothek.server.Book> books = new ArrayList<>();
		for (BookStore bookStore : bookStorageAdress.values()) {
			List<fr.umlv.bibliothek.server.Book> bookByTitle = bookStore
					.searchBookByTitle(title);
			if (!bookByTitle.isEmpty()) {
				books.addAll(bookByTitle);
			}
		}
		List<Book> localBooks = getServiceBookListFromRMIBookList(books);
		return localBooks.toArray(new Book[localBooks.size()]);
	}

	@Override
	public Book[] searchBookByKeyWords(String keyWord) throws RemoteException {

		List<fr.umlv.bibliothek.server.Book> books = new ArrayList<>();
		for (BookStore bookStore : bookStorageAdress.values()) {
			List<fr.umlv.bibliothek.server.Book> bookByTitle = bookStore
					.searchBookByKeyWords(keyWord.toUpperCase());
			if (!bookByTitle.isEmpty()) {
				books.addAll(bookByTitle);
			}
		}
		List<Book> localBooks = getServiceBookListFromRMIBookList(books);
		return localBooks.toArray(new Book[localBooks.size()]);
	}

	@Override
	public Book[] searchBookByPrice(double price) throws RemoteException {
		List<fr.umlv.bibliothek.server.Book> books = new ArrayList<>();
		for (BookStore bookStore : bookStorageAdress.values()) {
			List<fr.umlv.bibliothek.server.Book> bookByTitle = bookStore
					.searchBookByPrice(price);
			if (!bookByTitle.isEmpty()) {
				books.addAll(bookByTitle);
			}
		}
		List<Book> localBooks = getServiceBookListFromRMIBookList(books);
		return localBooks.toArray(new Book[localBooks.size()]);
	}

	@Override
	public Book[] searchBookByCategory(String category) throws RemoteException {

		BookStore bookStore = bookStorageAdress.get(category);
		if (Objects.isNull(bookStore)) {
			return null;
		}
		List<fr.umlv.bibliothek.server.Book> books = bookStore.getAllBooks();

		List<Book> localBooks = getServiceBookListFromRMIBookList(books);
		return localBooks.toArray(new Book[localBooks.size()]);
	}

	@Override
	public String[] getCategories() throws RemoteException {
		return bookStorageAdress.keySet().toArray(
				new String[bookStorageAdress.size()]);
	}

	@Override
	public Book[] getAllBooks() throws RemoteException {
		List<Book> allBooks = new ArrayList<>();
		for (String category : bookStorageAdress.keySet()) {
			BookStore bookStore = bookStorageAdress.get(category);
			List<Book> allBooksByCategory = getServiceBookListFromRMIBookList(bookStore
					.getAllBooks());
			allBooks.addAll(allBooksByCategory);

		}
		return allBooks.toArray(new Book[allBooks.size()]);
	}

	@Override
	public Book[] getAllBooksByCategory(String category) throws RemoteException {
		BookStore bookStore = bookStorageAdress.get(category);
		List<Book> allBooks = getServiceBookListFromRMIBookList(bookStore
				.getAllBooks());
		return allBooks.toArray(new Book[allBooks.size()]);
	}

	private BookStore getBookStoreByISBN(Long isbn) throws RemoteException {

		Collection<BookStore> bookStores = bookStorageAdress.values();

		for (BookStore bookStore : bookStores) {
			if (bookStore.getBookByISBN(isbn) != null) {
				return bookStore;
			}
		}
		return null;
	}

	private List<Book> getServiceBookListFromRMIBookList(
			List<fr.umlv.bibliothek.server.Book> books) {
		List<Book> bookList = new ArrayList<Book>();
		for (fr.umlv.bibliothek.server.Book b : books) {
			bookList.add(new Book(b));
		}
		return bookList;

	}

	@Override
	public boolean isBookAvailable(Long isbn) throws RemoteException {
		return getBookStoreByISBN(isbn).isBookAvailable(isbn);
	}

	@Override
	public int getNumberOfAvailableBook(Long isbn) throws RemoteException {
		System.out.println(isbn);
		return getBookStoreByISBN(isbn).getNumberOfAvailableBook(isbn);
	}
}
