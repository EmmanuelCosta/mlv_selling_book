package fr.umlv.bibliothek.server;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface Book extends Remote {
	
	public String getTitle() throws RemoteException;

	
	public void setTitle(String title) throws RemoteException;

	
	public long getISBN() throws RemoteException;

	
	public void setISBN(long isbn) throws RemoteException;

	
	public String getAutor() throws RemoteException;

	
	public void setAuthor(String author) throws RemoteException;

	
	public String getDescription() throws RemoteException;

	
	public void setDescription(String description) throws RemoteException;

	
	public double getPrice() throws RemoteException;

	
	public void setPrice(double price) throws RemoteException;

	public String getCategory() throws RemoteException;

	
	public List<String> getKeyWords() throws RemoteException;

	/**
	 * it's toString method of the book
	 * 
	 * @return
	 * @throws RemoteException
	 */
	public String remoteToString() throws RemoteException;

}
