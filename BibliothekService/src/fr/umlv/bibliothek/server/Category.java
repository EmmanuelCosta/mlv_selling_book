package fr.umlv.bibliothek.server;


public interface Category  {
	public enum CategoryEnum {
		PROG_OBJ, WEB, HACK, ELECTRO, PROG_FONC, PROG_IMPERATIVE, UNCLASSIFIED;
	}

	public String getCategory();
}