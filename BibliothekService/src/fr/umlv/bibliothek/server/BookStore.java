package fr.umlv.bibliothek.server;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import fr.umlv.bibliothek.server.Category.CategoryEnum;

public interface BookStore extends Remote {

	/**
	 * create a single Book
	 * 
	 * @param isbn
	 * @param author
	 * @param title
	 * @param description
	 * @param price
	 * @param keywords
	 * @throws RemoteException
	 */
	public void createBook(Long isbn, String author, String title,
			String description, double price, String... keywords)
			throws RemoteException;

	/**
	 * remove one book of isbn given
	 * {@link IllegalArgumentException} if book not in the store
	 * 
	 * @param isbn
	 * @return
	 * @throws RemoteException
	 */
	public Book removeBookByISBN(long isbn) throws RemoteException;

	/**
	 * 
	 * create numberTocreate instance of the Book
	 * @param isbn
	 * @param author
	 * @param title
	 * @param description
	 * @param price
	 * @param numberToCreate
	 * @param keywords
	 * @throws RemoteException
	 */
	public void createBook(Long isbn, String author, String title,
			String description, double price, int numberToCreate,
			String... keywords) throws RemoteException;

	public void addBook(Long isbn, int numberToAdd) throws RemoteException;

	/**
	 * add one copie of  book which isbn is given in the store
	 * throw {@link IllegalArgumentException} the store didn't conatins a book corresponding to that isbn
	 * 
	 * @param isbn
	 * @throws RemoteException
	 */
	public void addBook(Long isbn) throws RemoteException;

	/**
	 * remove one copie of the book if it exist otherwise it 
	 * throw {@link IllegalArgumentException} 
	 * 
	 * @param isbn
	 * @param numberToRemove
	 * @return
	 * @throws RemoteException
	 */
	public Book removeBookByISBN(long isbn, int numberToRemove)
			throws RemoteException;
	
	/**
	 *get one copie of the book if it exist otherwise 
	 * throw {@link IllegalArgumentException} 
	 * @param isbn
	 * @return
	 * @throws RemoteException
	 */
	public Book getBookByISBN(long isbn) throws RemoteException;
	
	/**
	 * get all book written by the author
	 * @param author
	 * @return
	 * @throws RemoteException
	 */
	public List<Book> searchBookByAuthor(String author) throws RemoteException;
	
	/**
	 * get all books with this title
	 * @param title
	 * @return
	 * @throws RemoteException
	 */
	public List<Book> searchBookByTitle(String title) throws RemoteException;
	
	/**
	 * get all book associate to the given keyword
	 * @param keyWord
	 * @return
	 * @throws RemoteException
	 */
	public List<Book> searchBookByKeyWords(String keyWord)
			throws RemoteException;
	
	public List<Book> searchBookByCategory(CategoryEnum category)
			throws RemoteException;
	
	/**
	 * get back all books which coast at least price
	 * @param price
	 * @return
	 * @throws RemoteException
	 */
	public List<Book> searchBookByPrice(double price) throws RemoteException;
	
	/**
	 * get all the book in that storage
	 * @return
	 * @throws RemoteException
	 */
	public List<Book> getAllBooks() throws RemoteException;
	
	/**
	 * give the bookstore category
	 * @return
	 * @throws RemoteException
	 */
	public String getCategory() throws RemoteException;
	
	/**
	 * true if there is at least one copie of the given book
	 * 
	 * @param isbn
	 * @return
	 * @throws RemoteException
	 */
	public boolean isBookAvailable(Long isbn) throws RemoteException;
	
	/**
	 * give the number of copy of the book recognize by is isbn
	 * @param isbn
	 * @return
	 * @throws RemoteException
	 */
	public int getNumberOfAvailableBook(Long isbn) throws RemoteException;

	/**
	 * reset the book storage
	 * @throws RemoteException 
	 */
	public void resetBookStore() throws RemoteException;

}
