package fr.umlv.bibliothek;

import java.rmi.RemoteException;

import fr.umlv.bibliothek.server.BookStore;
import fr.umlv.bibliothek.service.Book;

public interface BibliothekManager {
	
	/**
	 * 
	 * @param isbn
	 * @param author
	 * @param title
	 * @param description
	 * @param category
	 * @param price
	 * @param keywords
	 * @throws RemoteException
	 */
	public void addBook(Long isbn, String author, String title,
			String description, String category, double price,
			String... keywords) throws RemoteException;
/**
 * 
 * @param isbn
 * @return
 * @throws RemoteException
 */
	public Book removeBookByISBN(long isbn) throws RemoteException;

	/**
	 * add the specify number of copy in the store
	 * @param isbn
	 * @param author
	 * @param title
	 * @param description
	 * @param category
	 * @param price
	 * @param numberToAdd
	 * @param keywords
	 * @throws RemoteException
	 */
	public void addSpecificNumberOfBookInstance(Long isbn, String author, String title,
			String description, String category,double price, int numberToAdd,
			String... keywords) throws RemoteException;

	/**
	 * remove the specify numbr of copy of book in the store
	 * @param isbn
	 * @param numberToRemove
	 * @return
	 * @throws RemoteException
	 */
	public Book removeSpecificNumberOfBookInstanceByISBN(long isbn, int numberToRemove)
			throws RemoteException;

	/**
	 * give back the book mapped with the isbn
	 * @param isbn
	 * @return
	 * @throws RemoteException
	 */
	public Book getBookByISBN(long isbn) throws RemoteException;

	public Book[] searchBookByAuthor(String author) throws RemoteException;

	public Book[] searchBookByTitle(String title) throws RemoteException;

	public Book[] searchBookByKeyWords(String keyWord) throws RemoteException;

	public Book[] searchBookByCategory(String category) throws RemoteException;

	public Book[] searchBookByPrice(double price) throws RemoteException;

	public String[] getCategories() throws RemoteException;

	public Book[] getAllBooks() throws RemoteException;

	public Book[] getAllBooksByCategory(String category) throws RemoteException;

	public boolean isBookAvailable(Long isbn) throws RemoteException;

	public int getNumberOfAvailableBook(Long isbn) throws RemoteException;
}