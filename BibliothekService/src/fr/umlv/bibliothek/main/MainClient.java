package fr.umlv.bibliothek.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.util.Random;

import fr.umlv.bibliothek.BibliothekManagerImpl;
import fr.umlv.bibliothek.server.BookStore;

public class MainClient {
	public String getResourceFile() {
		return getClass().getClassLoader().getResource("bookStoreList")
				.getFile();
	}

	public static void main(String[] args) throws IOException {

		long LOWER_RANGE = 0; // assign lower range value
		long UPPER_RANGE = 5000

		; // assign upper range value
		Random random = new Random();

		long randomValue = 0;
		String file = new MainClient().getResourceFile();
		BufferedReader bookStoreFile = new BufferedReader(new FileReader(
				new File(file)));
		
		for (String bookStoreURL = bookStoreFile.readLine(); bookStoreURL != null; bookStoreURL = bookStoreFile
				.readLine()) {
			System.setProperty("java.security.policy", "sec.policy");
			if (System.getSecurityManager() == null) {
				System.setSecurityManager(new RMISecurityManager());
			}
			System.out.println("try to populate " + bookStoreURL);
			try {
				

				BookStore bookStore = (BookStore) Naming.lookup(bookStoreURL);
				if (bookStoreURL
						.equalsIgnoreCase("rmi://localhost:1099/BOOKSTOREDEFAULT_PROG_OBJ")) {
					randomValue = LOWER_RANGE
							+ (long) (random.nextDouble() * (UPPER_RANGE - LOWER_RANGE));
					bookStore
							.createBook(
									randomValue,
									"Claude",
									"Programmer en Java",
									"L'apprentissage du langage se fait en quatre etapes : apprentissage de la syntaxe de base, \nmaitrise de la programmation objet en Java, \ninitiation a la programmation graphique et enementielle avec la bibliotheque Swing, \n introduction au developpement Web avec les servlets Java, \n les JSP et JDBC.",
									36.40,5, "Java", "Debutant");

					randomValue = LOWER_RANGE
							+ (long) (random.nextDouble() * (UPPER_RANGE - LOWER_RANGE));

					bookStore
							.createBook(
									randomValue,
									"Aurelie",
									"Developper des services REST en Java",
									"Ce livre presente l'architecture d'un service REST, dans un contexte de developpement Java, avec des echanges en JSON. \nIl s'adresse a des developpeurs Java, qui connaissent ou non les applications JEE. \nIl a pour objectif de devenir une reference permettant au developpeur de partir de zero \net d'avoir l'ensemble des outils necessaires, dans leur derniere version, pour Debuter un projet, ou presenter un POC.",
									39.90,10, "Java", "REST");

					randomValue = LOWER_RANGE
							+ (long) (random.nextDouble() * (UPPER_RANGE - LOWER_RANGE));
					bookStore
							.createBook(
									randomValue,
									"Cyrille",
									"Aprenez a programmer en Java",
									"Debutant en Java ? Le seul prerequis est de savoir allumer son ordinateur ! Aucune connaissance en programmation n'est requise."
											+ " \nUne difficulte progressive pour ne perdre aucun lecteur en route.",
									60,3, "Java", "ZERO");

					randomValue = LOWER_RANGE
							+ (long) (random.nextDouble() * (UPPER_RANGE - LOWER_RANGE));
					bookStore
							.createBook(
									randomValue,
									"Francois",
									"JSF 2 avec Eclipse",
									"Ce livre expose la mise en oeuvre de la technologie Java Server Faces (JSF) sous l'environnement dedeveloppement Eclipse.\n Les aspects theoriques, etayes par de nombreux exemples, \nmontrent comment l'usage de composants JSF facilite la creation d'applications web basiques sur les technologies Java,\n en respectant le concept MVC.",
									50, 8,"Java", "Eclipse", "Jsf");

					randomValue = LOWER_RANGE
							+ (long) (random.nextDouble() * (UPPER_RANGE - LOWER_RANGE));
					bookStore
							.createBook(
									randomValue,
									"Annick",
									"Architectures reparties en Java",
									"Une architecture logicielle repartie suppose des donnees differentes "
											+ "et des touches differentes qui sont traitees sur des machines differentes.\n Une telle architecture entraine bien sur des problemes de transmission de donnees et de synchronisation de processus.\n Le langage Java permet de resoudre ces questions notamment dans le monde industriel. \nLe but de cet ouvrage est de donner les cles qui permettront de definir la solution la mieux adaptee a chaque situation.",
									60.25,100, "Java", "Soap", "RMI", "CORBA");

					randomValue = LOWER_RANGE
							+ (long) (random.nextDouble() * (UPPER_RANGE - LOWER_RANGE));
					bookStore
							.createBook(
									randomValue,
									" christopher",
									"C# sous Windows Phone",
									"Ce livre sur C# et Windows Phone (en versions 8 et 8.1 dans ces pages) s'adresse a tous les developpeurs qui souhaitent se lancer dans le developpement d'une application Windows Phone.\n Aucune connaissance des outils et langages de developpement Microsoft n'est necessaire.",
									29.25,20, "C#", "Store");

					randomValue = LOWER_RANGE
							+ (long) (random.nextDouble() * (UPPER_RANGE - LOWER_RANGE));
					bookStore
							.createBook(
									randomValue,
									"Bernard",
									"Le Mac pas a pas pour les nuls ",
									"Un livre indispensable pour reussir le premier contact \navec son nouveau compagnon qu'il soit portable ou de bureau ",
									26.99, "MAC");

					

					randomValue = LOWER_RANGE
							+ (long) (random.nextDouble() * (UPPER_RANGE - LOWER_RANGE));
					bookStore
							.createBook(
									randomValue,
									"Jaron",
									"Java vs C# : qui pousse de notre futur ? ",
									"Le choc des titans Java vs C# : expliquez comparez mis a nu",
									25.00,8, "Java", "C#");

					randomValue = LOWER_RANGE
							+ (long) (random.nextDouble() * (UPPER_RANGE - LOWER_RANGE));
					bookStore.createBook(randomValue, "Diane",
							"Scala",
							"Pas vraiment un nouveau venu",
							14.99,17, "Scala");

					randomValue = LOWER_RANGE
							+ (long) (random.nextDouble() * (UPPER_RANGE - LOWER_RANGE));
					bookStore.createBook(randomValue, "Mathieu",
							"Apprenez a programmer en C++",
							"Apprendre a en C pour les debutants", 26.00, 8,"C++");

					randomValue = LOWER_RANGE
							+ (long) (random.nextDouble() * (UPPER_RANGE - LOWER_RANGE));
					bookStore
							.createBook(
									randomValue,
									"Mathieu",
									"Programmer objet en c c'est possible ",
									"Comprendre les theorie de base pour programmer objet en c",
									26.00,25,"C", "Systeme", "Reseau", "Linux");

					randomValue = LOWER_RANGE
							+ (long) (random.nextDouble() * (UPPER_RANGE - LOWER_RANGE));
					bookStore
							.createBook(
									randomValue,
									"Simon",
									"Programmer un Raspberry Pi en python",
									"Le but de ce court ouvrage est de vous montrer\n comment creer des programmes et des jeux amusants \nsur votre Raspberry Pi en utilisant le langage Python",
									15.90,30, "Python");

					randomValue = LOWER_RANGE
							+ (long) (random.nextDouble() * (UPPER_RANGE - LOWER_RANGE));
					bookStore
							.createBook(
									randomValue,
									"John",
									"Programmation VBA pour Excel 2010 et 2013 pour les nuls ",
									"Apprendre a programmer en VBA quand on est debutant",
									22.95,2, "VBA", "Excel");

					randomValue = LOWER_RANGE
							+ (long) (random.nextDouble() * (UPPER_RANGE - LOWER_RANGE));
					bookStore
							.createBook(
									randomValue,
									"Victor",
									"Programmez en oriente objet en PHP",
									"Vous codez en PHP et vous voulez decouvrir \nou approfondir vos connaissances en oriente objet ?\nCe livre est fait pour vous !\n Il vous permettra de découvrir une nouvelle facon de concevoir vos projets,\n pour une maintenance et une distribution de votre code plus faciles !",
									28.00,6, "PHP", "PROG_OBJ");

					randomValue = LOWER_RANGE
							+ (long) (random.nextDouble() * (UPPER_RANGE - LOWER_RANGE));
					bookStore
							.createBook(
									randomValue,
									"Remy",
									"Initiation a l'algorithmique et a la programmation en C++",
									"Cours et exercices corriges pour apprendre a programmer en C++\n. Comprendre les principes de base de l'agorithmique.",
									22.90,5, "c++", "Algorithmique");

					System.out.println("Populate " + bookStoreURL);

				}else if (bookStoreURL
						.equalsIgnoreCase("rmi://localhost:1099/BOOKSTOREDEFAULT_WEB")) {
					randomValue = LOWER_RANGE
							+ (long) (random.nextDouble() * (UPPER_RANGE - LOWER_RANGE));
					bookStore
							.createBook(
									randomValue,
									"Alexandra",
									"Technique de referencement Web",
									"c'est un vrai manuel de programmation pour referenceur.\n S'ajoutent a cela deux parties indispensables pour gerer l'audit et le suivi SEO d'un site web. \nUn ouvrage indispensable pour les chefs de projet web, referenceurs et developpeurs !",
									26.40,8, "Web", "SEO");
					randomValue = LOWER_RANGE
							+ (long) (random.nextDouble() * (UPPER_RANGE - LOWER_RANGE));
					
					bookStore
					.createBook(
							randomValue,
							"Camille",
							"FabLabs",
							"Les nouveaux lieux de fabrications numeriques",
							6.40,2, "Web");
					
					randomValue = LOWER_RANGE
							+ (long) (random.nextDouble() * (UPPER_RANGE - LOWER_RANGE));
					bookStore
					.createBook(
							randomValue,
							"Raphael",
							"Memento CSS3",
							"Le css3 simplement",
							10.40,10, "Web","zero");
					
					randomValue = LOWER_RANGE
							+ (long) (random.nextDouble() * (UPPER_RANGE - LOWER_RANGE));
					bookStore.createBook(randomValue, "Bernard",
							"Realisez votre site web avec HTML5 et CSS3",
							"Creer son site web n'a jamais ete aussi facile !",
							25.00, 2,"Web", "HTML5", "CSS3");
					
					System.out.println("Populate " + bookStoreURL);
					
				}else if (bookStoreURL
						.equalsIgnoreCase("rmi://localhost:1099/BOOKSTOREDEFAULT_HACK")) {
					randomValue = LOWER_RANGE
							+ (long) (random.nextDouble() * (UPPER_RANGE - LOWER_RANGE));
					bookStore
							.createBook(
									randomValue,
									"Cedric",
									"Securite et espionnage informatique",
									"Connaissance de la menace APT (Advanced Persistent Threat) et du cyberespionnage",
									26.40, 15,"Hack","Securite");
					
					randomValue = LOWER_RANGE
							+ (long) (random.nextDouble() * (UPPER_RANGE - LOWER_RANGE));
					bookStore
							.createBook(
									randomValue,
									"Cedric",
									"Les bases du hacking",
									"Les bases du hacking est une introduction aux techniques de hacking et aux tests d'intrusion",
									86.40, 20,"Hack");
					
					randomValue = LOWER_RANGE
							+ (long) (random.nextDouble() * (UPPER_RANGE - LOWER_RANGE));
					bookStore
							.createBook(
									randomValue,
									"Jhon",
									"Technique de hacking",
									"Les hackers n'ont de cesse de repousser les limites, d'explorer l'inconnu et de faire evoluer leur science.\n c'est connaissance sont a vous",
									206,13, "Hack");
					
					System.out.println("Populate " + bookStoreURL);
				}
			} catch (Exception e) {
				System.out.println(bookStoreURL + "  unreacheable");
			}
		}
		
		
		
	}
}
