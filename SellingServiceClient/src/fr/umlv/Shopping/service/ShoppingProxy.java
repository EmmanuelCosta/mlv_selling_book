package fr.umlv.Shopping.service;

public class ShoppingProxy implements fr.umlv.Shopping.service.Shopping {
  private String _endpoint = null;
  private fr.umlv.Shopping.service.Shopping shopping = null;
  
  public ShoppingProxy() {
    _initShoppingProxy();
  }
  
  public ShoppingProxy(String endpoint) {
    _endpoint = endpoint;
    _initShoppingProxy();
  }
  
  private void _initShoppingProxy() {
    try {
      shopping = (new fr.umlv.Shopping.service.ShoppingServiceLocator()).getShopping();
      if (shopping != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)shopping)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)shopping)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (shopping != null)
      ((javax.xml.rpc.Stub)shopping)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public fr.umlv.Shopping.service.Shopping getShopping() {
    if (shopping == null)
      _initShoppingProxy();
    return shopping;
  }
  
  public java.lang.String getCurrency() throws java.rmi.RemoteException{
    if (shopping == null)
      _initShoppingProxy();
    return shopping.getCurrency();
  }
  
  public void setCurrency(java.lang.String currency_dest) throws java.rmi.RemoteException{
    if (shopping == null)
      _initShoppingProxy();
    shopping.setCurrency(currency_dest);
  }
  
  public void clearTheCart() throws java.rmi.RemoteException{
    if (shopping == null)
      _initShoppingProxy();
    shopping.clearTheCart();
  }
  
  public int getNumberOfAvailableByBISBNBook(long isbn) throws java.rmi.RemoteException{
    if (shopping == null)
      _initShoppingProxy();
    return shopping.getNumberOfAvailableByBISBNBook(isbn);
  }
  
  public void addBookByISBN(long isbn) throws java.rmi.RemoteException{
    if (shopping == null)
      _initShoppingProxy();
    shopping.addBookByISBN(isbn);
  }
  
  public void createUserAccount(java.lang.String login, java.lang.String password, java.lang.String firstname, java.lang.String name, java.lang.String email, java.lang.String birthDate, java.lang.String address) throws java.rmi.RemoteException{
    if (shopping == null)
      _initShoppingProxy();
    shopping.createUserAccount(login, password, firstname, name, email, birthDate, address);
  }
  
  public fr.umlv.Shopping.service.Book[] searchBookByAuthor(java.lang.String author) throws java.rmi.RemoteException{
    if (shopping == null)
      _initShoppingProxy();
    return shopping.searchBookByAuthor(author);
  }
  
  public fr.umlv.Shopping.service.Book[] getAllBookByCategory(java.lang.String category) throws java.rmi.RemoteException{
    if (shopping == null)
      _initShoppingProxy();
    return shopping.getAllBookByCategory(category);
  }
  
  public fr.umlv.Shopping.service.Book[] searchBookByTitle(java.lang.String title) throws java.rmi.RemoteException{
    if (shopping == null)
      _initShoppingProxy();
    return shopping.searchBookByTitle(title);
  }
  
  public fr.umlv.Shopping.service.Book[] searchBookByPrice(double price) throws java.rmi.RemoteException{
    if (shopping == null)
      _initShoppingProxy();
    return shopping.searchBookByPrice(price);
  }
  
  public fr.umlv.Shopping.service.Book[] searchBookByCategory(java.lang.String category) throws java.rmi.RemoteException{
    if (shopping == null)
      _initShoppingProxy();
    return shopping.searchBookByCategory(category);
  }
  
  public fr.umlv.Shopping.service.Book[] searchBookByKeyWords(java.lang.String keyWord) throws java.rmi.RemoteException{
    if (shopping == null)
      _initShoppingProxy();
    return shopping.searchBookByKeyWords(keyWord);
  }
  
  public boolean removeBookFromCartByISBN(long isbn) throws java.rmi.RemoteException{
    if (shopping == null)
      _initShoppingProxy();
    return shopping.removeBookFromCartByISBN(isbn);
  }
  
  public boolean addBookIntoCartByISBN(long isbn) throws java.rmi.RemoteException{
    if (shopping == null)
      _initShoppingProxy();
    return shopping.addBookIntoCartByISBN(isbn);
  }
  
  public fr.umlv.Shopping.service.ShoppingCart getShopcart() throws java.rmi.RemoteException{
    if (shopping == null)
      _initShoppingProxy();
    return shopping.getShopcart();
  }
  
  public fr.umlv.Shopping.service.Book removeBookByISBN(long isbn) throws java.rmi.RemoteException{
    if (shopping == null)
      _initShoppingProxy();
    return shopping.removeBookByISBN(isbn);
  }
  
  public fr.umlv.Shopping.service.UserManager getUm() throws java.rmi.RemoteException{
    if (shopping == null)
      _initShoppingProxy();
    return shopping.getUm();
  }
  
  public fr.umlv.Shopping.service.BankManager getBank() throws java.rmi.RemoteException{
    if (shopping == null)
      _initShoppingProxy();
    return shopping.getBank();
  }
  
  public boolean sellBook(fr.umlv.Shopping.service.Book b, long num_account, java.lang.String name, java.lang.String firstname, java.lang.String currency) throws java.rmi.RemoteException{
    if (shopping == null)
      _initShoppingProxy();
    return shopping.sellBook(b, num_account, name, firstname, currency);
  }
  
  public fr.umlv.Shopping.service.Book[] getAllBook() throws java.rmi.RemoteException{
    if (shopping == null)
      _initShoppingProxy();
    return shopping.getAllBook();
  }
  
  public boolean tryToConnect(java.lang.String login, java.lang.String password) throws java.rmi.RemoteException{
    if (shopping == null)
      _initShoppingProxy();
    return shopping.tryToConnect(login, password);
  }
  
  public boolean buyCart(long num_account, java.lang.String login, java.lang.String password, java.lang.String firstname, java.lang.String name) throws java.rmi.RemoteException{
    if (shopping == null)
      _initShoppingProxy();
    return shopping.buyCart(num_account, login, password, firstname, name);
  }
  
  public java.lang.String getLoggin() throws java.rmi.RemoteException{
    if (shopping == null)
      _initShoppingProxy();
    return shopping.getLoggin();
  }
  
  public void setMdp(java.lang.String mdp) throws java.rmi.RemoteException{
    if (shopping == null)
      _initShoppingProxy();
    shopping.setMdp(mdp);
  }
  
  public boolean tryToDeconnect(java.lang.String login, java.lang.String password) throws java.rmi.RemoteException{
    if (shopping == null)
      _initShoppingProxy();
    return shopping.tryToDeconnect(login, password);
  }
  
  public double convertMoney(double amount, java.lang.String from, java.lang.String to) throws java.rmi.RemoteException{
    if (shopping == null)
      _initShoppingProxy();
    return shopping.convertMoney(amount, from, to);
  }
  
  public java.lang.String getMdp() throws java.rmi.RemoteException{
    if (shopping == null)
      _initShoppingProxy();
    return shopping.getMdp();
  }
  
  public void setLoggin(java.lang.String loggin) throws java.rmi.RemoteException{
    if (shopping == null)
      _initShoppingProxy();
    shopping.setLoggin(loggin);
  }
  
  public fr.umlv.Shopping.service.Book getBookByIsbn(long isbn) throws java.rmi.RemoteException{
    if (shopping == null)
      _initShoppingProxy();
    return shopping.getBookByIsbn(isbn);
  }
  
  
}