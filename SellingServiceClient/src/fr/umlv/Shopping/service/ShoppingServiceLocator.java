/**
 * ShoppingServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package fr.umlv.Shopping.service;

public class ShoppingServiceLocator extends org.apache.axis.client.Service implements fr.umlv.Shopping.service.ShoppingService {

    public ShoppingServiceLocator() {
    }


    public ShoppingServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ShoppingServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for Shopping
    private java.lang.String Shopping_address = "http://localhost:8080/BookSellingService/services/Shopping";

    public java.lang.String getShoppingAddress() {
        return Shopping_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ShoppingWSDDServiceName = "Shopping";

    public java.lang.String getShoppingWSDDServiceName() {
        return ShoppingWSDDServiceName;
    }

    public void setShoppingWSDDServiceName(java.lang.String name) {
        ShoppingWSDDServiceName = name;
    }

    public fr.umlv.Shopping.service.Shopping getShopping() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(Shopping_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getShopping(endpoint);
    }

    public fr.umlv.Shopping.service.Shopping getShopping(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            fr.umlv.Shopping.service.ShoppingSoapBindingStub _stub = new fr.umlv.Shopping.service.ShoppingSoapBindingStub(portAddress, this);
            _stub.setPortName(getShoppingWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setShoppingEndpointAddress(java.lang.String address) {
        Shopping_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (fr.umlv.Shopping.service.Shopping.class.isAssignableFrom(serviceEndpointInterface)) {
                fr.umlv.Shopping.service.ShoppingSoapBindingStub _stub = new fr.umlv.Shopping.service.ShoppingSoapBindingStub(new java.net.URL(Shopping_address), this);
                _stub.setPortName(getShoppingWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("Shopping".equals(inputPortName)) {
            return getShopping();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://service.Shopping.umlv.fr", "ShoppingService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://service.Shopping.umlv.fr", "Shopping"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("Shopping".equals(portName)) {
            setShoppingEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
