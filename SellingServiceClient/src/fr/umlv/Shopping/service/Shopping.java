/**
 * Shopping.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package fr.umlv.Shopping.service;

public interface Shopping extends java.rmi.Remote {
    public java.lang.String getCurrency() throws java.rmi.RemoteException;
    public void setCurrency(java.lang.String currency_dest) throws java.rmi.RemoteException;
    public void clearTheCart() throws java.rmi.RemoteException;
    public int getNumberOfAvailableByBISBNBook(long isbn) throws java.rmi.RemoteException;
    public void addBookByISBN(long isbn) throws java.rmi.RemoteException;
    public void createUserAccount(java.lang.String login, java.lang.String password, java.lang.String firstname, java.lang.String name, java.lang.String email, java.lang.String birthDate, java.lang.String address) throws java.rmi.RemoteException;
    public fr.umlv.Shopping.service.Book[] searchBookByAuthor(java.lang.String author) throws java.rmi.RemoteException;
    public fr.umlv.Shopping.service.Book[] getAllBookByCategory(java.lang.String category) throws java.rmi.RemoteException;
    public fr.umlv.Shopping.service.Book[] searchBookByTitle(java.lang.String title) throws java.rmi.RemoteException;
    public fr.umlv.Shopping.service.Book[] searchBookByPrice(double price) throws java.rmi.RemoteException;
    public fr.umlv.Shopping.service.Book[] searchBookByCategory(java.lang.String category) throws java.rmi.RemoteException;
    public fr.umlv.Shopping.service.Book[] searchBookByKeyWords(java.lang.String keyWord) throws java.rmi.RemoteException;
    public boolean removeBookFromCartByISBN(long isbn) throws java.rmi.RemoteException;
    public boolean addBookIntoCartByISBN(long isbn) throws java.rmi.RemoteException;
    public fr.umlv.Shopping.service.ShoppingCart getShopcart() throws java.rmi.RemoteException;
    public fr.umlv.Shopping.service.Book removeBookByISBN(long isbn) throws java.rmi.RemoteException;
    public fr.umlv.Shopping.service.UserManager getUm() throws java.rmi.RemoteException;
    public fr.umlv.Shopping.service.BankManager getBank() throws java.rmi.RemoteException;
    public boolean sellBook(fr.umlv.Shopping.service.Book b, long num_account, java.lang.String name, java.lang.String firstname, java.lang.String currency) throws java.rmi.RemoteException;
    public fr.umlv.Shopping.service.Book[] getAllBook() throws java.rmi.RemoteException;
    public boolean tryToConnect(java.lang.String login, java.lang.String password) throws java.rmi.RemoteException;
    public boolean buyCart(long num_account, java.lang.String login, java.lang.String password, java.lang.String firstname, java.lang.String name) throws java.rmi.RemoteException;
    public java.lang.String getLoggin() throws java.rmi.RemoteException;
    public void setMdp(java.lang.String mdp) throws java.rmi.RemoteException;
    public boolean tryToDeconnect(java.lang.String login, java.lang.String password) throws java.rmi.RemoteException;
    public double convertMoney(double amount, java.lang.String from, java.lang.String to) throws java.rmi.RemoteException;
    public java.lang.String getMdp() throws java.rmi.RemoteException;
    public void setLoggin(java.lang.String loggin) throws java.rmi.RemoteException;
    public fr.umlv.Shopping.service.Book getBookByIsbn(long isbn) throws java.rmi.RemoteException;
}
