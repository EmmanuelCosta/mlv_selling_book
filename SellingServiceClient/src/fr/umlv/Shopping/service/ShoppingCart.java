/**
 * ShoppingCart.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package fr.umlv.Shopping.service;

public class ShoppingCart  implements java.io.Serializable {
    private long booknumber;

    private fr.umlv.Shopping.service.Book[] cart;

    private double totalPrice;

    public ShoppingCart() {
    }

    public ShoppingCart(
           long booknumber,
           fr.umlv.Shopping.service.Book[] cart,
           double totalPrice) {
           this.booknumber = booknumber;
           this.cart = cart;
           this.totalPrice = totalPrice;
    }


    /**
     * Gets the booknumber value for this ShoppingCart.
     * 
     * @return booknumber
     */
    public long getBooknumber() {
        return booknumber;
    }


    /**
     * Sets the booknumber value for this ShoppingCart.
     * 
     * @param booknumber
     */
    public void setBooknumber(long booknumber) {
        this.booknumber = booknumber;
    }


    /**
     * Gets the cart value for this ShoppingCart.
     * 
     * @return cart
     */
    public fr.umlv.Shopping.service.Book[] getCart() {
        return cart;
    }


    /**
     * Sets the cart value for this ShoppingCart.
     * 
     * @param cart
     */
    public void setCart(fr.umlv.Shopping.service.Book[] cart) {
        this.cart = cart;
    }


    /**
     * Gets the totalPrice value for this ShoppingCart.
     * 
     * @return totalPrice
     */
    public double getTotalPrice() {
        return totalPrice;
    }


    /**
     * Sets the totalPrice value for this ShoppingCart.
     * 
     * @param totalPrice
     */
    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ShoppingCart)) return false;
        ShoppingCart other = (ShoppingCart) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.booknumber == other.getBooknumber() &&
            ((this.cart==null && other.getCart()==null) || 
             (this.cart!=null &&
              java.util.Arrays.equals(this.cart, other.getCart()))) &&
            this.totalPrice == other.getTotalPrice();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += new Long(getBooknumber()).hashCode();
        if (getCart() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCart());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCart(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += new Double(getTotalPrice()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ShoppingCart.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://service.Shopping.umlv.fr", "ShoppingCart"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("booknumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://service.Shopping.umlv.fr", "booknumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cart");
        elemField.setXmlName(new javax.xml.namespace.QName("http://service.Shopping.umlv.fr", "cart"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://service.Shopping.umlv.fr", "Book"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://service.Shopping.umlv.fr", "item"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalPrice");
        elemField.setXmlName(new javax.xml.namespace.QName("http://service.Shopping.umlv.fr", "totalPrice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
