/**
 * ShoppingService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package fr.umlv.Shopping.service;

public interface ShoppingService extends javax.xml.rpc.Service {
    public java.lang.String getShoppingAddress();

    public fr.umlv.Shopping.service.Shopping getShopping() throws javax.xml.rpc.ServiceException;

    public fr.umlv.Shopping.service.Shopping getShopping(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
