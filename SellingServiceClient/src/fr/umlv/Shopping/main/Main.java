package fr.umlv.Shopping.main;

import java.io.IOException;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.xml.rpc.ServiceException;

import fr.umlv.Shopping.service.Shopping;
import fr.umlv.Shopping.service.ShoppingServiceLocator;
import fr.umlv.Shopping.service.ShoppingSoapBindingStub;
import fr.umlv.ihm.ShoppingUI;

public class Main {
	public static void main(String[] args) throws ServiceException, IOException {

		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
//				System.out
//						.println("Metal Nimbus CDE/Motif Windows Windows Classic");
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			try {
				UIManager.setLookAndFeel(UIManager
						.getSystemLookAndFeelClassName());
			} catch (ClassNotFoundException | InstantiationException
					| IllegalAccessException | UnsupportedLookAndFeelException ef) {

			}
		}
		
		
		Shopping shopping = new ShoppingServiceLocator().getShopping();
		((ShoppingSoapBindingStub)shopping).setMaintainSession(true);
		ShoppingUI shoppingUI = new ShoppingUI(shopping);

	}
}
