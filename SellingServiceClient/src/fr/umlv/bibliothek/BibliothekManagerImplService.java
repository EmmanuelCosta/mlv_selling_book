/**
 * BibliothekManagerImplService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package fr.umlv.bibliothek;

public interface BibliothekManagerImplService extends javax.xml.rpc.Service {
    public java.lang.String getBibliothekManagerImplAddress();

    public fr.umlv.bibliothek.BibliothekManagerImpl getBibliothekManagerImpl() throws javax.xml.rpc.ServiceException;

    public fr.umlv.bibliothek.BibliothekManagerImpl getBibliothekManagerImpl(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
