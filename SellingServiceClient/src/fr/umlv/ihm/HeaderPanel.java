package fr.umlv.ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.rmi.RemoteException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.umlv.Shopping.service.Shopping;

public class HeaderPanel extends JPanel {

	private Shopping shopping;
	private BufferedImage image;

	/**
	 * 
	 * @param shopping
	 * @throws IOException
	 */
	public HeaderPanel(Shopping shopping) throws IOException {
		this.shopping = shopping;
		try {
			image = ImageIO.read(getClass().getClassLoader().getResource(
					"livres3.jpg"));
		} catch (IOException ex) {

		}
		initHeaderPanel();

	}

	/**
	 * 
	 * @throws IOException
	 */
	private void initHeaderPanel() throws IOException {
		setLayout(new BorderLayout());

		JPanel pan1 = new JPanel();
		pan1.setLayout(new FlowLayout(FlowLayout.RIGHT));
		
		// BOUTON CONNECTION
		final JButton connexion = new JButton("Se Connecter");
		connexion.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (connexion.getText().equals("Se Deconnecter")) {
					Boolean tryToConnect;
					try {
						tryToConnect = shopping.tryToConnect(shopping.getLoggin(),
								shopping.getMdp());
						connexion.setText("Se Connecter");
						if (tryToConnect) {
							
						}
					} catch (RemoteException e1) {
					}
					
				} else {
					new ConnexionUI(shopping, connexion);
				}
			}
		});
		
		// BOUTON ENREGISTRER
		JButton enregistrer = new JButton("S'inscrire");
		enregistrer.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				RegistrationUI registrationUI = new RegistrationUI(shopping);
				
			}
		});
		
		// BOUTON CONSULTER SON PANIER
		JButton panier = new JButton("Consulter Panier");
		panier.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				BuyCartJFrame cart;
				try {
					cart = new BuyCartJFrame(shopping);
					cart.displayCartContent();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});

		pan1.setBackground(Color.decode("#7E5835"));

		panier.setBackground(Color.decode("#E0CDA9"));
		enregistrer.setBackground(Color.decode("#E0CDA9"));
		connexion.setBackground(Color.decode("#E0CDA9"));

		JPanel livrePanel = new JPanel();
		livrePanel.setLayout(new GridLayout(0,1));
		livrePanel.add(new JLabel("Bienvenue Sur notre service de vente de livres"));
		pan1.add(connexion);
		pan1.add(enregistrer);
		pan1.add(panier);
		new JLabel("Vous n'etes pas connect�");
		this.add(pan1, BorderLayout.SOUTH);
		this.setBackground(Color.decode("#7E5835"));
	}

	@Override
	public void paint(Graphics g) {
		// TODO Auto-generated method stub
		super.paint(g);
		g.drawImage(image, 0, 0, null);
	}

}
