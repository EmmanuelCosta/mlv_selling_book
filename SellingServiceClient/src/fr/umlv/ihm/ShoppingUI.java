package fr.umlv.ihm;

import java.awt.BorderLayout;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;
import javax.xml.rpc.ServiceException;

import fr.umlv.Shopping.service.Shopping;
import fr.umlv.Shopping.service.ShoppingServiceLocator;
import fr.umlv.Shopping.service.ShoppingSoapBindingStub;

public class ShoppingUI extends JFrame {

	private JPanel header;
	private JPanel research;
	private JPanel result;
	private JPanel footer;
	private JSplitPane splitHeaderAndOther, splitResearchResultAndFooter,
			splitResearchResult;
	private Shopping shopping;

	public ShoppingUI(Shopping shopping) throws IOException, ServiceException {
		this.shopping = shopping;
		this.header = new HeaderPanel(shopping);
		this.footer = new FooterPanel();
		this.result = new ResultPanel(shopping);
		this.research = new ResearchPanel(shopping, (ResultPanel) result);
		init();
	}

	private void init() {

		this.setTitle("Selling Service");
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocation(0, 0);
		this.setSize(1200, 700);

		splitResearchResult = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
				result, research);
		splitResearchResult.setDividerLocation(0.7);
		splitResearchResult.setDividerSize(1);

		splitResearchResultAndFooter = new JSplitPane(
				JSplitPane.VERTICAL_SPLIT, splitResearchResult, footer);

		splitResearchResultAndFooter.setDividerLocation(0.9);
		splitResearchResultAndFooter.setDividerSize(1);

		splitHeaderAndOther = new JSplitPane(JSplitPane.VERTICAL_SPLIT, header,
				splitResearchResultAndFooter);
		splitHeaderAndOther.setDividerLocation(112);
		splitHeaderAndOther.setDividerSize(1);
		splitHeaderAndOther.setEnabled(false);
		this.getContentPane().add(splitHeaderAndOther, BorderLayout.CENTER);

		this.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				splitResearchResult.setDividerLocation(0.7);
				splitResearchResultAndFooter.setDividerLocation(0.9);
			}
		});
		this.setVisible(true);
	}

	public JPanel getResult() {
		return result;
	}

}
