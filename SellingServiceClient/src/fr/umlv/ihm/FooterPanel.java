package fr.umlv.ihm;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class FooterPanel extends JPanel {
	public FooterPanel() {
		init();
	}

	private void init() {
		GridLayout gridLayout = new GridLayout(0,1);
		this.setLayout(gridLayout);
	
		JLabel comp = new JLabel("PAR EMMANUEL BABALA COSTA - KARINE SIETTE & SYBILLE CRIMET ");
		this.add(comp);
		comp.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);	
		this.add(comp);
		comp = new JLabel("ETUDIANT EN MASTER 2 LOGICIEL");
		this.add(comp);
		comp.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		this.add(comp);
		comp = new JLabel("UPEM");
		comp.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		this.add(comp);

	}

}
