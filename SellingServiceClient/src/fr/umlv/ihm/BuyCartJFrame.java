package fr.umlv.ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.util.Objects;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import fr.umlv.Shopping.service.Book;
import fr.umlv.Shopping.service.Shopping;
import fr.umlv.Shopping.service.ShoppingCart;

public class BuyCartJFrame extends JFrame {

	private Shopping shopping;
	private JPanel north;
	private JPanel south;
	private GridLayout gridLayout;

	public BuyCartJFrame(Shopping shopping) throws RemoteException {
		this.shopping = shopping;
		gridLayout = new GridLayout(0, 3, 1, 1);
		north = new JPanel(gridLayout);
		south = new JPanel();
		this.setLayout(new BorderLayout());

		// INITIALISATION DE LA FENETRE
		this.setTitle("Consultation de votre panier");
		this.setSize(600, 600);
		this.setLocationRelativeTo(null);
		// this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// this.setContentPane(north);
		// this.setContentPane(south);
		this.setVisible(true);

		// INITIALISATION DU CONTENU
		initBuyCartJFrame();
	}

	private void initBuyCartJFrame() throws RemoteException {

		// INITIALISATION DE LA LISTE DES ARTICLES DU PANIER
		JLabel comp = new JLabel("Titre");
		comp.setHorizontalAlignment(JLabel.CENTER);
		comp.setVerticalAlignment(JLabel.CENTER);
		comp.setBorder(BorderFactory.createLineBorder(Color.black));
		comp.setOpaque(true);
		north.add(comp);

		comp = new JLabel("Prix");
		comp.setHorizontalAlignment(JLabel.CENTER);
		comp.setVerticalAlignment(JLabel.CENTER);
		comp.setOpaque(true);
		comp.setBorder(BorderFactory.createLineBorder(Color.black));
		north.add(comp);

		comp = new JLabel("-");
		comp.setHorizontalAlignment(JLabel.CENTER);
		comp.setVerticalAlignment(JLabel.CENTER);
		comp.setOpaque(true);
		comp.setBorder(BorderFactory.createLineBorder(Color.black));
		north.add(comp);

		// INITIALISATION DU MONTANT TOTAL DU PANIER
		String currency = "EUR";
		try {
			currency = shopping.getCurrency();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		double price = shopping.getShopcart().getTotalPrice();
		if (!currency.equals("EUR")) {
			try {
				price = shopping.convertMoney(price, "EUR", currency);
			} catch (RemoteException e1) {
				currency = "The convertor service is unreacheable so the price is display on EURO";
			}
		}
		
		comp = new JLabel("Montant total : "
				+ price + " "
				+ shopping.getCurrency() + "     ");
		comp.setHorizontalAlignment(JLabel.CENTER);
		comp.setVerticalAlignment(JLabel.CENTER);
		comp.setOpaque(true);
		// comp.setBorder(BorderFactory.createLineBorder(Color.black));
		south.add(comp);

		// INITIALISATION DU BOUTON D'ACHAT
		JButton buy = new JButton("Acheter");
		buy.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				BuyJFrame buycart = null;
				try {
					if (shopping.getShopcart().getBooknumber() == 0) {
						StringBuilder msg = new StringBuilder();
						int reponse = JOptionPane
								.showConfirmDialog(
										null,
										"Vous ne pouvez pas acheter de livre car\n vous n'en avez pas dans votre panier.",
										"Achat impossible",
										JOptionPane.OK_CANCEL_OPTION,
										JOptionPane.PLAIN_MESSAGE);
						if (reponse == JOptionPane.OK_CANCEL_OPTION) {
							dispose();
						}
					} else {
						buycart = new BuyJFrame(shopping);
						try {
							initBuyCartJFrame();
						} catch (RemoteException e1) {

						}
					}
				} catch (HeadlessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		south.add(new JLabel(" "));
		south.add(buy);

		this.add(north, BorderLayout.NORTH);
		this.add(south, BorderLayout.SOUTH);

	}

	public void displayCartContent() throws RemoteException {
		String currency = "EUR";
		try {
			currency = shopping.getCurrency();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		north.removeAll();
		south.removeAll();
		initBuyCartJFrame();
		ShoppingCart shopcart = shopping.getShopcart();
		Book[] cart = shopcart.getCart();
		if (!Objects.isNull(cart)) {
			for (final Book b : cart) {
				JLabel comp = new JLabel(b.getTitle());
				comp.setHorizontalAlignment(JLabel.CENTER);
				comp.setVerticalAlignment(JLabel.CENTER);
				comp.setOpaque(true);
				comp.setBackground(Color.white);
				comp.setBorder(BorderFactory.createLineBorder(Color.black));
				north.add(comp);

				double price = b.getPrice();
				if (!currency.equals("EUR")) {
					try {
						price = shopping.convertMoney(price, "EUR", currency);
					} catch (RemoteException e1) {
						currency = "The convertor service is unreacheable so the price is display on EURO";
					}
				}
				
				comp = new JLabel(String.valueOf(price));
				comp.setHorizontalAlignment(JLabel.CENTER);
				comp.setVerticalAlignment(JLabel.CENTER);
				comp.setOpaque(true);
				comp.setBackground(Color.white);
				comp.setBorder(BorderFactory.createLineBorder(Color.black));
				north.add(comp);

				JButton less = new JButton("Retirer");
				less.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						new StringBuilder();

						int reponse = JOptionPane
								.showConfirmDialog(
										null,
										"Etes-vous sur de vouloir supprimer ce livre du panier ?",
										"Supprimer ?",
										JOptionPane.YES_NO_OPTION,
										JOptionPane.PLAIN_MESSAGE);
						if (reponse == JOptionPane.YES_OPTION) {
							try {
								shopping.removeBookFromCartByISBN(b.getISBN());
								displayCartContent();
							} catch (RemoteException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}

					}
				});
				north.add(less);
			}
			putEmptyLine();
			north.updateUI();
		}
	}

	/**
	 * permet de sauter une ligne
	 */
	private void putEmptyLine() {
		JLabel empty = new JLabel("");
		north.add(empty);
		empty = new JLabel("");
		north.add(empty);
		empty = new JLabel("");
		north.add(empty);
	}
}
