package fr.umlv.ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fr.umlv.Shopping.service.Book;
import fr.umlv.Shopping.service.Shopping;

public class BuyJFrame extends JFrame {
	private Shopping shopping;
	private JPanel south;
	private JPanel validation;
	private GridLayout gridLayout;

	public BuyJFrame(Shopping shopping) {
		this.shopping = shopping;
		gridLayout = new GridLayout(0, 2, 1, 1);
		south = new JPanel(gridLayout);
		validation = new JPanel();
		this.setLayout(new BorderLayout());

		// INITIALISATION DE LA FENETRE
		this.setTitle("Entrer vos ID Bancaire");
		this.setSize(400, 200);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setVisible(true);

		// INITIALISATION DU CONTENU
		initBuyJFrame();
	}

	private void initBuyJFrame() {

		// INITIALISATION DE LA LISTE DES CHAMPS A REMPLIR POUR ACHETER SON
		// PANIER
		JLabel comp = new JLabel("Nom");
		comp.setHorizontalAlignment(JLabel.CENTER);
		comp.setVerticalAlignment(JLabel.CENTER);
		comp.setOpaque(true);
		comp.setBorder(BorderFactory.createLineBorder(Color.black));
		south.add(comp);

		final JTextField name = new JTextField();
		name.setPreferredSize(new Dimension(comp.getSize().width, 25));
		name.setText("");
		name.setBackground(Color.WHITE);
		south.add(name);

		comp = new JLabel("Prenom");
		comp.setHorizontalAlignment(JLabel.CENTER);
		comp.setVerticalAlignment(JLabel.CENTER);
		comp.setOpaque(true);
		comp.setBorder(BorderFactory.createLineBorder(Color.black));
		south.add(comp);

		final JTextField firstName = new JTextField();
		firstName.setPreferredSize(new Dimension(comp.getSize().width, 25));
		firstName.setText("");
		firstName.setBackground(Color.WHITE);
		south.add(firstName);

		comp = new JLabel("Numero de compte");
		comp.setHorizontalAlignment(JLabel.CENTER);
		comp.setVerticalAlignment(JLabel.CENTER);
		comp.setOpaque(true);
		comp.setBorder(BorderFactory.createLineBorder(Color.black));
		south.add(comp);

		final JTextField num_account = new JTextField();
		num_account.setPreferredSize(new Dimension(comp.getSize().width, 25));
		num_account.setText("");
		num_account.setBackground(Color.WHITE);
		south.add(num_account);

		JButton validate = new JButton("Valider");
		validate.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					boolean isbuy = shopping.buyCart(
							Long.parseLong(num_account.getText()),
							shopping.getLoggin(), shopping.getMdp(),
							firstName.getText(), name.getText());
					if (isbuy) {
						int reponse = JOptionPane.showConfirmDialog(null,
								"Votre commande a bien ete effectue!",
								"Commande validee",
								JOptionPane.OK_CANCEL_OPTION,
								JOptionPane.PLAIN_MESSAGE);
						if (reponse == JOptionPane.OK_OPTION) {
							shopping.clearTheCart();

						} else if (reponse == JOptionPane.CANCEL_OPTION) {

						}
					} else {
						int reponse = JOptionPane
								.showConfirmDialog(
										null,
										"Votre commande n'a pas ete effectuee \n peut etre pour plusieurs raisons : \n\t- soit vous n'etes pas connecte \n\t- soit votre solde banquaire est insufffisant.!",
										"Commande non effectuee",
										JOptionPane.OK_CANCEL_OPTION,
										JOptionPane.PLAIN_MESSAGE);
						if (reponse == JOptionPane.OK_CANCEL_OPTION) {
							setVisible(false);
							dispose();
						}
					}
				} catch (NumberFormatException | RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		validation.add(new JLabel(""));
		validation.add(validate);
		this.add(south, BorderLayout.NORTH);
		this.add(validation, BorderLayout.CENTER);
	}
}
