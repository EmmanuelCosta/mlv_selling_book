package fr.umlv.ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fr.umlv.Shopping.service.Shopping;

public class ConnexionUI extends JFrame {
	private final Shopping shopping;
	private final JButton buttonConnexion;
	private JPanel connexionPanel;
	private JPanel connect;
	private GridLayout gridLayout;

	public ConnexionUI(Shopping shopping, JButton buttonConnexion) {
		this.shopping = shopping;
		this.buttonConnexion = buttonConnexion;
		this.setTitle("Creation d'un compte utilisateur");
		this.setSize(350, 120);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setVisible(true);
		gridLayout = new GridLayout(0, 2, 1, 1);
		connexionPanel = new JPanel(gridLayout);
		connect = new JPanel();
		this.setLayout(new BorderLayout());

		initConnexionUI();
	}

	private void initConnexionUI() {

		JLabel comp = new JLabel("Login");
		comp.setHorizontalAlignment(JLabel.CENTER);
		comp.setVerticalAlignment(JLabel.CENTER);
		comp.setOpaque(true);
		connexionPanel.add(comp);

		final JTextField login = new JTextField();
		login.setPreferredSize(new Dimension(comp.getSize().width, 25));
		login.setText("");
		login.setBackground(Color.WHITE);
		connexionPanel.add(login);

		comp = new JLabel("Mot de passe");
		comp.setHorizontalAlignment(JLabel.CENTER);
		comp.setVerticalAlignment(JLabel.CENTER);
		comp.setOpaque(true);
		connexionPanel.add(comp);

		final JTextField mdpField = new JTextField();
		mdpField.setPreferredSize(new Dimension(comp.getSize().width, 25));
		mdpField.setText("");
		mdpField.setBackground(Color.WHITE);
		connexionPanel.add(mdpField);

		final JButton connexionButton = new JButton("Connexion");
		connexionButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					String motDPasse = mdpField.getText();
					String loggin = login.getText();
					Boolean tryToConnect = shopping.tryToConnect(loggin,
							motDPasse);
					if (loggin.isEmpty() || motDPasse.isEmpty()) {

						int reponse = JOptionPane
								.showConfirmDialog(
										null,
										"Vous devez remplir tous les champs pour pouvoir vous connecter",
										"Erreur de saisie !",
										JOptionPane.OK_CANCEL_OPTION,
										JOptionPane.PLAIN_MESSAGE);
						if (reponse == JOptionPane.OK_CANCEL_OPTION) {
							setVisible(false);
						}
					} else {
						if (tryToConnect) {
							shopping.setLoggin(loggin);
							shopping.setMdp(motDPasse);
							if (buttonConnexion.getText()
									.equals("Se Connecter")) {
								buttonConnexion.setText("Se Deconnecter");
							}
							dispose();
						}
					}
				} catch (RemoteException e1) {

				}
			}
		});
		buttonConnexion.setHorizontalAlignment(JButton.CENTER);
		connect.add(new JLabel(""));
		connect.add(connexionButton);
		this.add(connexionPanel, BorderLayout.NORTH);
		this.add(connect, BorderLayout.CENTER);
		setVisible(true);
	}

}
