package fr.umlv.ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import fr.umlv.Shopping.service.Book;
import fr.umlv.Shopping.service.Shopping;

public class ResultPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private GridLayout gridLayout;
	private JPanel central;

	private List<Book> internalBook = new ArrayList<>();
	private Shopping shopping;
	private final int ITEMS_PER_PAGE = 13;

	public ResultPanel() {
		gridLayout = new GridLayout(0, 5, 1, 1);
		central = new JPanel(gridLayout);
		this.setLayout(new BorderLayout());
		initHeader();
	}

	public ResultPanel(Shopping shopping) {
		this();
		this.shopping = shopping;
	}

	/**
	 * place les en tetes
	 */
	private void initHeader() {
		this.setBackground(Color.decode("#D0C07A"));
		central.setBackground(Color.decode("#D0C07A"));
		JLabel comp = new JLabel("Titre");
		comp.setHorizontalAlignment(JLabel.CENTER);
		comp.setVerticalAlignment(JLabel.CENTER);
		comp.setBorder(BorderFactory.createLineBorder(Color.black));
		comp.setOpaque(true);
		// comp.setBackground(Color.white);
		central.add(comp);
		
		
		comp = new JLabel("Auteur");
		comp.setHorizontalAlignment(JLabel.CENTER);
		comp.setVerticalAlignment(JLabel.CENTER);
		comp.setOpaque(true);
		// comp.setBackground(Color.white);
		comp.setBorder(BorderFactory.createLineBorder(Color.black));
		central.add(comp);

		comp = new JLabel("Categorie");
		comp.setHorizontalAlignment(JLabel.CENTER);
		comp.setVerticalAlignment(JLabel.CENTER);
		comp.setOpaque(true);
		// comp.setBackground(Color.white);
		comp.setBorder(BorderFactory.createLineBorder(Color.black));
		central.add(comp);

		comp = new JLabel("Prix");
		comp.setHorizontalAlignment(JLabel.CENTER);
		comp.setVerticalAlignment(JLabel.CENTER);
		comp.setOpaque(true);
		// comp.setBackground(Color.white);
		comp.setBorder(BorderFactory.createLineBorder(Color.black));
		central.add(comp);

		comp = new JLabel("+");
		comp.setHorizontalAlignment(JLabel.CENTER);
		comp.setVerticalAlignment(JLabel.CENTER);
		comp.setOpaque(true);
		// comp.setBackground(Color.white);
		comp.setBorder(BorderFactory.createLineBorder(Color.black));
		central.add(comp);
		this.add(central, BorderLayout.NORTH);

	}

	/**
	 * permet afficher les livres en parametres sur le panel
	 * 
	 * @param book
	 */
	public void onResearchDone(Book[] book) {
		onResearchDone(Arrays.asList(book));
	}

	/**
	 * permet afficher les livres en parametres sur le panel
	 * 
	 * @param book
	 */
	public void onResearchDone(List book) {

		internalBook = new ArrayList<Book>(book);
		displayTheList(0, ITEMS_PER_PAGE);
	}

	/**
	 * effectue l'affichage des livres dans la listes internes en partant de
	 * l'indice begin jusqu'a l'indice end
	 * 
	 * @param begin
	 * @param end
	 */

	private void displayTheList(int begin, int end) {
		String currency = "EUR";
		try {
			currency = shopping.getCurrency();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		central.removeAll();
		initHeader();
		revalidate();
		int size = internalBook.size();
		if (size < end) {
			end = size;
		}
		for (final Book b : internalBook.subList(begin, end)) {
			JLabel comp = new JLabel(b.getTitle());
			comp.setHorizontalAlignment(JLabel.CENTER);
			comp.setVerticalAlignment(JLabel.CENTER);
			comp.setOpaque(true);
			comp.setBackground(Color.white);
			comp.setBorder(BorderFactory.createLineBorder(Color.black));
			central.add(comp);

			comp = new JLabel(b.getAutor());
			comp.setHorizontalAlignment(JLabel.CENTER);
			comp.setVerticalAlignment(JLabel.CENTER);
			comp.setOpaque(true);
			comp.setBackground(Color.white);
			comp.setBorder(BorderFactory.createLineBorder(Color.black));
			central.add(comp);

			comp = new JLabel(b.getCategory());
			comp.setHorizontalAlignment(JLabel.CENTER);
			comp.setVerticalAlignment(JLabel.CENTER);
			comp.setOpaque(true);
			comp.setBackground(Color.white);
			comp.setBorder(BorderFactory.createLineBorder(Color.black));
			central.add(comp);

			double price = b.getPrice();
			if (!currency.equals("EUR")) {
				try {
					price = shopping.convertMoney(price, "EUR", currency);
				} catch (RemoteException e1) {
					currency = " the convertor service is unreacheable so the price is display on EURO";
				}
			}

			comp = new JLabel(price + "");
			comp.setHorizontalAlignment(JLabel.CENTER);
			comp.setVerticalAlignment(JLabel.CENTER);
			comp.setOpaque(true);
			comp.setBackground(Color.white);
			comp.setBorder(BorderFactory.createLineBorder(Color.black));
			central.add(comp);

			JButton more = new JButton("ACHETER");
			more.setBackground(Color.decode("#AE8964"));
			more.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					String currency;
					try {
						currency = shopping.getCurrency();

						int numberOfAvailableBook = 0;
						try {
							numberOfAvailableBook = shopping
									.getNumberOfAvailableByBISBNBook(b
											.getISBN());
						} catch (RemoteException e2) {
							System.err.println("====>e2 " + e2.getCause());
						}
						double price = b.getPrice();
						if (currency != "EUR") {
							try {
								price = shopping.convertMoney(price, "EUR",
										currency);
							} catch (RemoteException e1) {
								currency = " the convertor service is unreacheable so the price is display on EURO";
							}
						}
						StringBuilder msg = new StringBuilder();
						msg.append("ISBN : ").append(b.getISBN())
								.append("\nTitre : ").append(b.getTitle())
								.append("\nAuteur : ").append(b.getAutor())
								.append("\nCategory : ")
								.append(b.getCategory())
								.append("\nDescription : ")
								.append(b.getDescription()).append("\nPrix : ")
								.append(price + " ").append(currency)
								.append("\nExemplaires Restants : ")
								.append(numberOfAvailableBook);

						int reponse = JOptionPane.showConfirmDialog(null,
								msg.toString(), "Ajouter ce livre au panier ?",
								JOptionPane.YES_NO_OPTION,
								JOptionPane.PLAIN_MESSAGE);
						if (reponse == JOptionPane.YES_OPTION) {
							shopping.addBookIntoCartByISBN(b.getISBN());

						}
					} catch (RemoteException e3) {
						System.err.println("====>e3 " + e3);
					}

				}
			});

			central.add(more);

		}

		putEmptyLine();
		putNavigationButton(begin, end, size);
		updatePanel();

	}

	/**
	 * permet de placer les boutons precedent et suivant
	 * 
	 * @param begin
	 * @param end
	 * @param size
	 */
	private void putNavigationButton(int begin, int end, int size) {
		final int next = end;
		final int previous = begin;
		boolean isBeginSet = false;
		if (begin > 0) {
			JButton precedent = new JButton("PRECEDENT");
			precedent.setBackground(Color.GREEN);
			precedent.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					displayTheList(previous - ITEMS_PER_PAGE, previous);
				}
			});
			central.add(precedent);
			isBeginSet = true;
		}
		JLabel empty = new JLabel("");
		if (!isBeginSet) {

			central.add(empty);
		}
		empty = new JLabel("");
		central.add(empty);
		empty = new JLabel("");
		central.add(empty);
		empty = new JLabel("");
		central.add(empty);
		if (end < size) {
			JButton suivant = new JButton("SUIVANT");
			suivant.setBackground(Color.GREEN);

			suivant.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					displayTheList(next, next + ITEMS_PER_PAGE);
				}
			});
			central.add(suivant);
		}
	}

	/**
	 * demande de mise a jour du panel
	 */
	private void updatePanel() {
		this.updateUI();
	}

	/**
	 * permet de sauter une ligne
	 */
	private void putEmptyLine() {
		JLabel empty = new JLabel("");
		central.add(empty);
		empty = new JLabel("");
		central.add(empty);
		empty = new JLabel("");
		central.add(empty);
		empty = new JLabel("");
		central.add(empty);
		empty = new JLabel("");
		central.add(empty);
	}
}
