package fr.umlv.ihm;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;

import javax.swing.AbstractButton;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import fr.umlv.Shopping.service.Book;
import fr.umlv.Shopping.service.Shopping;

public class ResearchPanel extends JPanel {

	private final Shopping shopping;
	private JLabel label = new JLabel("Research");
	private final String[] allDevice = new String[] { "EUR", "USD", "GBP",
			"JPY", "CNY", "ALL", "RUB" };
	private ResultPanel resultPanel;

	public ResearchPanel(Shopping shopping, ResultPanel resultPanel) {
		this.shopping = shopping;
		this.resultPanel = resultPanel;
		init();
	}

	private void init() {
		this.setBackground(Color.decode("#D0C07A"));
		Box box = new Box(BoxLayout.Y_AXIS);
		box.setAlignmentX(JComponent.CENTER_ALIGNMENT);
		box.add(Box.createVerticalGlue());
		add(box);

		GridLayout layout = new GridLayout(0, 1);
		layout.setVgap(4);

		JPanel panel = new JPanel(layout);
		panel.setBackground(Color.decode("#D0C07A"));

		panel.add(new JLabel("  "));
		JLabel comp = new JLabel("Veuillez entrer les elements a rechercher");
		comp.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		// comp.setPreferredSize(new Dimension(15, 20));
		panel.add(comp);
		final JTextField rechercheZone = new JTextField(
				"Tout coche recupere tous les livres");
		rechercheZone.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
			}

			@Override
			public void focusGained(FocusEvent e) {
				rechercheZone.setBackground(Color.WHITE);

			}
		});
		rechercheZone.setPreferredSize(new Dimension(comp.getSize().width, 25));
		panel.add(rechercheZone);
		box.add(panel);

		JPanel criteria = new JPanel(new GridLayout(0, 2));
		criteria.setBackground(Color.decode("#D0C07A"));
		final ButtonGroup buttonGroup = new ButtonGroup();
		JRadioButton author = new JRadioButton("Auteur");
		JRadioButton title = new JRadioButton("Titre");
		JRadioButton keyword = new JRadioButton("Mots-cles");
		JRadioButton category = new JRadioButton("Categorie");
		JRadioButton all = new JRadioButton("Tout");
		all.setBackground(Color.decode("#D0C07A"));
		title.setBackground(Color.decode("#D0C07A"));
		keyword.setBackground(Color.decode("#D0C07A"));
		author.setBackground(Color.decode("#D0C07A"));
		category.setBackground(Color.decode("#D0C07A"));
		
		all.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				rechercheZone.setText("Tout coche recupere tous les livres");
			}
		});

		ActionListener putBlanc = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String text = rechercheZone.getText();
				if (text.equals("Tout coche recupere tous les livres"))
					rechercheZone.setText("");
			}
		};
		title.addActionListener(putBlanc);
		author.addActionListener(putBlanc);
		category.addActionListener(putBlanc);
		keyword.addActionListener(putBlanc);

		all.setSelected(true);
		buttonGroup.add(title);
		buttonGroup.add(keyword);
		buttonGroup.add(category);
		buttonGroup.add(author);
		buttonGroup.add(all);

		criteria.add(all);
		criteria.add(author);
		criteria.add(title);
		criteria.add(category);
		criteria.add(keyword);

		JPanel price = new JPanel(new GridLayout(0, 2));
		price.setBackground(Color.decode("#D0C07A"));
		final JTextField prixField = new JTextField("Tous les prix");
		prixField.setEditable(false);

		prixField.addFocusListener(new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
			}

			@Override
			public void focusGained(FocusEvent e) {
				prixField.setText("");
				prixField.setBackground(Color.WHITE);
			}
		});
		final JCheckBox prixCheckBox = new JCheckBox("Indiquez un Prix");
		prixCheckBox.setBackground(Color.decode("#D0C07A"));
		prixCheckBox.addActionListener(new ActionListener() {
			int i = 0;

			@Override
			public void actionPerformed(ActionEvent e) {
				if (i % 2 == 0) {
					prixField.setEditable(true);
					prixField.setText("");
					i = 1;
				} else {
					prixField.setEditable(false);
					prixField.setText("Tous les prix");
					i = 2;
				}
			}
		});

		final JComboBox<String> alldevicesElt = new JComboBox<String>(allDevice);
		alldevicesElt.setBackground(Color.decode("#D0C07A"));
		price.add(prixCheckBox);
		price.add(prixField);
		price.add(new JLabel("Devises en"));
		// price.add(new JLabel("Devises "));
		price.add(alldevicesElt);
		box.add(criteria);
		box.add(price);
		box.add(new JLabel(" "));
		JButton researchButton = new JButton("Rechercher");
		researchButton.setBackground(Color.decode("#C8AD7F"));
		researchButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String critere = "";
				String prixText = "";
				String devise = "";
				boolean error = false;
				String text = rechercheZone.getText();
				Object[] selectedObjects = buttonGroup.getSelection()
						.getSelectedObjects();

				for (Enumeration<AbstractButton> buttons = buttonGroup
						.getElements(); buttons.hasMoreElements();) {
					AbstractButton button = buttons.nextElement();

					if (button.isSelected()) {
						critere = button.getText();
					}
				}

				if (text.isEmpty()) {
					rechercheZone.setBackground(Color.RED);
					error = true;
				}
				if (prixCheckBox.isSelected()) {
					prixText = prixField.getText();
					if (prixText.isEmpty() || prixText == "") {
						prixField.setBackground(Color.RED);
						error = true;
					} else if (containsChar(prixText)) {
						prixField.setBackground(Color.RED);
						prixField.setText("Chiffres seulement");
						error = true;
					}
				}
				if (!error) {
					devise = alldevicesElt.getItemAt(alldevicesElt
							.getSelectedIndex());
					List<Book> books = new ArrayList<Book>();
					List<Book> filteredBook = new ArrayList<Book>();
					boolean filtredePrix = false;
					Integer priceSet = 100000;
					try {

						if (prixCheckBox.isSelected()) {

							priceSet = new Integer(prixText);
							if (critere.equalsIgnoreCase("Mots-cles")) {
								filtredePrix = true;
							} else {
								Book[] bookByPrice = shopping
										.searchBookByPrice(priceSet);
								if (!Objects.isNull(bookByPrice)) {

									List<Book> asList = Arrays
											.asList(bookByPrice);
									books.addAll(asList);

									filtredePrix = true;
								}
							}

						}
						shopping.setCurrency(devise);
						switch (critere) {
						case "Auteur":
							if (filtredePrix) {
								for (Book b : books) {
									if (b.getAutor().equalsIgnoreCase(text)) {
										filteredBook.add(b);
									}
								}
							} else {
								Book[] searchBookByAut = shopping
										.searchBookByAuthor(text);
								if (!Objects.isNull(searchBookByAut)) {
									List<Book> asList = Arrays
											.asList(searchBookByAut);
									books.addAll(asList);
								}
							}
							break;
						case "Titre":
							if (filtredePrix) {
								for (Book b : books) {
									if (b.getTitle().equalsIgnoreCase(text)) {
										filteredBook.add(b);
									}
								}
							} else {
								Book[] searchBookByTitle = shopping
										.searchBookByTitle(text);
								if (!Objects.isNull(searchBookByTitle)) {
									List<Book> asList = Arrays
											.asList(searchBookByTitle);
									books.addAll(asList);
								} else {
									JOptionPane
											.showMessageDialog(
													null,
													(String) "Pas de livres correspondant a vos criteres");
								}
							}
							break;
						case "Categorie":
							if (filtredePrix) {
								for (Book b : books) {
									if (b.getCategory().equalsIgnoreCase(text)) {
										filteredBook.add(b);
									}
								}
							} else {
								Book[] searchBookBycat = shopping
										.searchBookByCategory(text);
								if (!Objects.isNull(searchBookBycat)) {
									List<Book> asList = Arrays
											.asList(searchBookBycat);
									books.addAll(asList);
								} else {
									JOptionPane
											.showMessageDialog(
													null,
													(String) "Pas de livres correspondant a vos criteres");
								}
							}
							break;
						case "Mots-cles":

							Book[] searchBookByKeyWords = shopping
									.searchBookByKeyWords(text);
							if (!Objects.isNull(searchBookByKeyWords)) {
								List<Book> asList = Arrays
										.asList(searchBookByKeyWords);

								if (filtredePrix) {
									for (Book b : asList) {
										if (b.getPrice() <= priceSet) {
											filteredBook.add(b);
										}
									}

								} else {
									books.addAll(asList);
								}

							}

							break;
						default:
							if (filtredePrix) {
								filtredePrix = false;
								break;
							}

							Book[] allBook = shopping.getAllBook();
							if (!Objects.isNull(allBook)) {
								books.addAll(Arrays.asList(allBook));
							} else {
								JOptionPane
										.showMessageDialog(
												null,
												(String) "Pas de livres correspondant a dans la Bibliotheque");
							}
						}
						if (filtredePrix) {							
							resultPanel.onResearchDone(filteredBook);
						} else {
							resultPanel.onResearchDone(books);
						}

					} catch (RemoteException e1) {
						JOptionPane
								.showMessageDialog(
										null,
										(String) "Pas de livres correspondant a vos criteres");
					}

					// if (prixCheckBox.isSelected()) {
					// Integer price = new Integer(prixText);
					// for (Book b : books) {
					// if (b.getPrice() <= price) {
					// filteredBook.add(b);
					// }
					// resultPanel.onResearchDone(filteredBook);
					// }
					// } else {
					// resultPanel.onResearchDone(books);
					// }
				}

			}
		});

		box.add(researchButton);
	}

	private boolean containsChar(String s) {
		String regex = "[0-9]+";
		return !s.matches(regex);
	}
}
