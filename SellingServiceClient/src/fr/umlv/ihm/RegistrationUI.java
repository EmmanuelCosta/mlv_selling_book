package fr.umlv.ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import fr.umlv.Shopping.service.Shopping;

public class RegistrationUI extends JFrame {

	private static final long serialVersionUID = 1L;
	private Shopping shopping;
	private JPanel center;
	private JPanel register;
	private GridLayout gridLayout;

	public RegistrationUI(Shopping shopping) {
		this.shopping = shopping;
		gridLayout = new GridLayout(0, 2, 1, 1);
		center = new JPanel(gridLayout);
		register = new JPanel();
		this.setLayout(new BorderLayout());

		// INITIALISATION DE LA FENETRE
		this.setTitle("Creation d'un compte utilisateur");
		this.setSize(400, 250);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setVisible(true);

		// INITIALISATION DU CONTENU
		initRegistrationUI();
	}

	private void initRegistrationUI() {
		JLabel comp = new JLabel("Login");
		comp.setHorizontalAlignment(JLabel.CENTER);
		comp.setVerticalAlignment(JLabel.CENTER);
		comp.setOpaque(true);
		center.add(comp);

		final JTextField login = new JTextField();
		login.setPreferredSize(new Dimension(comp.getSize().width, 25));
		login.setText("");
		login.setBackground(Color.WHITE);
		center.add(login);

		comp = new JLabel("Password");
		comp.setHorizontalAlignment(JLabel.CENTER);
		comp.setVerticalAlignment(JLabel.CENTER);
		comp.setOpaque(true);
		center.add(comp);

		final JTextField password = new JTextField();
		password.setPreferredSize(new Dimension(comp.getSize().width, 25));
		password.setText("");
		password.setBackground(Color.WHITE);
		center.add(password);

		comp = new JLabel("Nom");
		comp.setHorizontalAlignment(JLabel.CENTER);
		comp.setVerticalAlignment(JLabel.CENTER);
		comp.setOpaque(true);
		center.add(comp);

		final JTextField name = new JTextField();
		name.setPreferredSize(new Dimension(comp.getSize().width, 25));
		name.setText("");
		name.setBackground(Color.WHITE);
		center.add(name);

		comp = new JLabel("Prenom");
		comp.setHorizontalAlignment(JLabel.CENTER);
		comp.setVerticalAlignment(JLabel.CENTER);
		comp.setOpaque(true);
		center.add(comp);

		final JTextField firstName = new JTextField();
		firstName.setPreferredSize(new Dimension(comp.getSize().width, 25));
		firstName.setText("");
		firstName.setBackground(Color.WHITE);
		center.add(firstName);

		comp = new JLabel("E-mail");
		comp.setHorizontalAlignment(JLabel.CENTER);
		comp.setVerticalAlignment(JLabel.CENTER);
		comp.setOpaque(true);
		center.add(comp);

		final JTextField email = new JTextField();
		email.setPreferredSize(new Dimension(comp.getSize().width, 25));
		email.setText("");
		email.setBackground(Color.WHITE);
		center.add(email);

		comp = new JLabel("Date de naissance");
		comp.setHorizontalAlignment(JLabel.CENTER);
		comp.setVerticalAlignment(JLabel.CENTER);
		comp.setOpaque(true);
		center.add(comp);

		final JTextField birthDate = new JTextField();
		birthDate.setPreferredSize(new Dimension(comp.getSize().width, 25));
		birthDate.setText("");
		birthDate.setBackground(Color.WHITE);
		center.add(birthDate);

		comp = new JLabel("Adresse");
		comp.setHorizontalAlignment(JLabel.CENTER);
		comp.setVerticalAlignment(JLabel.CENTER);
		comp.setOpaque(true);
		center.add(comp);

		final JTextField address = new JTextField();
		address.setPreferredSize(new Dimension(comp.getSize().width, 25));
		address.setText("");
		address.setBackground(Color.WHITE);
		center.add(address);

		JButton registration = new JButton("Enregistrer");
		registration.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (login.getText().isEmpty() || password.getText().isEmpty()
							|| firstName.getText().isEmpty()
							|| name.getText().isEmpty()
							|| email.getText().isEmpty()
							|| birthDate.getText().isEmpty()
							|| address.getText().isEmpty()) {

						int reponse = JOptionPane.showConfirmDialog(
										null,
										"Vous devez remplir tous les champs pour pouvoir vous inscrire",
										"Erreur de saisie !",
										JOptionPane.OK_CANCEL_OPTION,
										JOptionPane.PLAIN_MESSAGE);
						if (reponse == JOptionPane.OK_CANCEL_OPTION) {
							setVisible(false);
							dispose();
						}
					} else {
						shopping.createUserAccount(login.getText(),
								password.getText(), firstName.getText(),
								name.getText(), email.getText(),
								birthDate.getText(), address.getText());
						int reponse = JOptionPane.showConfirmDialog(null,
								"Votre compte a bien ete cree !",
								"Nouvel utilisateur cree",
								JOptionPane.OK_CANCEL_OPTION,
								JOptionPane.PLAIN_MESSAGE);
						if (reponse == JOptionPane.OK_CANCEL_OPTION) {
							setVisible(false);
							dispose();
						}
					}
				} catch (RemoteException e1) {
					e1.printStackTrace();
				}
			}
		});
		register.add(new JLabel(""));
		register.add(registration);

		this.add(center, BorderLayout.CENTER);
		this.add(register, BorderLayout.SOUTH);

	}
}