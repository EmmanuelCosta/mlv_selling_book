/**
 * BibliothekManagerImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package fr.umlv.bibliothek;

public interface BibliothekManagerImpl extends java.rmi.Remote {
    public java.lang.String[] getCategories() throws java.rmi.RemoteException;
    public boolean isBookAvailable(long isbn) throws java.rmi.RemoteException;
    public void addBook(long isbn, java.lang.String author, java.lang.String title, java.lang.String description, java.lang.String category, double price, java.lang.String[] keywords) throws java.rmi.RemoteException;
    public fr.umlv.bibliothek.service.Book getBookByISBN(long isbn) throws java.rmi.RemoteException;
    public fr.umlv.bibliothek.service.Book removeBookByISBN(long isbn) throws java.rmi.RemoteException;
    public fr.umlv.bibliothek.service.Book[] getAllBooks() throws java.rmi.RemoteException;
    public fr.umlv.bibliothek.service.Book[] searchBookByKeyWords(java.lang.String keyWord) throws java.rmi.RemoteException;
    public fr.umlv.bibliothek.service.Book[] getAllBooksByCategory(java.lang.String category) throws java.rmi.RemoteException;
    public fr.umlv.bibliothek.service.Book[] searchBookByAuthor(java.lang.String author) throws java.rmi.RemoteException;
    public fr.umlv.bibliothek.service.Book[] searchBookByTitle(java.lang.String title) throws java.rmi.RemoteException;
    public fr.umlv.bibliothek.service.Book[] searchBookByPrice(double price) throws java.rmi.RemoteException;
    public fr.umlv.bibliothek.service.Book[] searchBookByCategory(java.lang.String category) throws java.rmi.RemoteException;
    public int getNumberOfAvailableBook(long isbn) throws java.rmi.RemoteException;
    public void addSpecificNumberOfBookInstance(long isbn, java.lang.String author, java.lang.String title, java.lang.String description, java.lang.String category, double price, int numberToAdd, java.lang.String[] keywords) throws java.rmi.RemoteException;
    public fr.umlv.bibliothek.service.Book removeSpecificNumberOfBookInstanceByISBN(long isbn, int numberToRemove) throws java.rmi.RemoteException;
}
