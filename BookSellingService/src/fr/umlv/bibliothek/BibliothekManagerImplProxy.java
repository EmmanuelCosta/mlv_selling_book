package fr.umlv.bibliothek;

public class BibliothekManagerImplProxy implements fr.umlv.bibliothek.BibliothekManagerImpl {
  private String _endpoint = null;
  private fr.umlv.bibliothek.BibliothekManagerImpl bibliothekManagerImpl = null;
  
  public BibliothekManagerImplProxy() {
    _initBibliothekManagerImplProxy();
  }
  
  public BibliothekManagerImplProxy(String endpoint) {
    _endpoint = endpoint;
    _initBibliothekManagerImplProxy();
  }
  
  private void _initBibliothekManagerImplProxy() {
    try {
      bibliothekManagerImpl = (new fr.umlv.bibliothek.BibliothekManagerImplServiceLocator()).getBibliothekManagerImpl();
      if (bibliothekManagerImpl != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bibliothekManagerImpl)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bibliothekManagerImpl)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bibliothekManagerImpl != null)
      ((javax.xml.rpc.Stub)bibliothekManagerImpl)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public fr.umlv.bibliothek.BibliothekManagerImpl getBibliothekManagerImpl() {
    if (bibliothekManagerImpl == null)
      _initBibliothekManagerImplProxy();
    return bibliothekManagerImpl;
  }
  
  public java.lang.String[] getCategories() throws java.rmi.RemoteException{
    if (bibliothekManagerImpl == null)
      _initBibliothekManagerImplProxy();
    return bibliothekManagerImpl.getCategories();
  }
  
  public boolean isBookAvailable(long isbn) throws java.rmi.RemoteException{
    if (bibliothekManagerImpl == null)
      _initBibliothekManagerImplProxy();
    return bibliothekManagerImpl.isBookAvailable(isbn);
  }
  
  public void addBook(long isbn, java.lang.String author, java.lang.String title, java.lang.String description, java.lang.String category, double price, java.lang.String[] keywords) throws java.rmi.RemoteException{
    if (bibliothekManagerImpl == null)
      _initBibliothekManagerImplProxy();
    bibliothekManagerImpl.addBook(isbn, author, title, description, category, price, keywords);
  }
  
  public fr.umlv.bibliothek.service.Book getBookByISBN(long isbn) throws java.rmi.RemoteException{
    if (bibliothekManagerImpl == null)
      _initBibliothekManagerImplProxy();
    return bibliothekManagerImpl.getBookByISBN(isbn);
  }
  
  public fr.umlv.bibliothek.service.Book removeBookByISBN(long isbn) throws java.rmi.RemoteException{
    if (bibliothekManagerImpl == null)
      _initBibliothekManagerImplProxy();
    return bibliothekManagerImpl.removeBookByISBN(isbn);
  }
  
  public fr.umlv.bibliothek.service.Book[] getAllBooks() throws java.rmi.RemoteException{
    if (bibliothekManagerImpl == null)
      _initBibliothekManagerImplProxy();
    return bibliothekManagerImpl.getAllBooks();
  }
  
  public fr.umlv.bibliothek.service.Book[] searchBookByKeyWords(java.lang.String keyWord) throws java.rmi.RemoteException{
    if (bibliothekManagerImpl == null)
      _initBibliothekManagerImplProxy();
    return bibliothekManagerImpl.searchBookByKeyWords(keyWord);
  }
  
  public fr.umlv.bibliothek.service.Book[] getAllBooksByCategory(java.lang.String category) throws java.rmi.RemoteException{
    if (bibliothekManagerImpl == null)
      _initBibliothekManagerImplProxy();
    return bibliothekManagerImpl.getAllBooksByCategory(category);
  }
  
  public fr.umlv.bibliothek.service.Book[] searchBookByAuthor(java.lang.String author) throws java.rmi.RemoteException{
    if (bibliothekManagerImpl == null)
      _initBibliothekManagerImplProxy();
    return bibliothekManagerImpl.searchBookByAuthor(author);
  }
  
  public fr.umlv.bibliothek.service.Book[] searchBookByTitle(java.lang.String title) throws java.rmi.RemoteException{
    if (bibliothekManagerImpl == null)
      _initBibliothekManagerImplProxy();
    return bibliothekManagerImpl.searchBookByTitle(title);
  }
  
  public fr.umlv.bibliothek.service.Book[] searchBookByPrice(double price) throws java.rmi.RemoteException{
    if (bibliothekManagerImpl == null)
      _initBibliothekManagerImplProxy();
    return bibliothekManagerImpl.searchBookByPrice(price);
  }
  
  public fr.umlv.bibliothek.service.Book[] searchBookByCategory(java.lang.String category) throws java.rmi.RemoteException{
    if (bibliothekManagerImpl == null)
      _initBibliothekManagerImplProxy();
    return bibliothekManagerImpl.searchBookByCategory(category);
  }
  
  public int getNumberOfAvailableBook(long isbn) throws java.rmi.RemoteException{
    if (bibliothekManagerImpl == null)
      _initBibliothekManagerImplProxy();
    return bibliothekManagerImpl.getNumberOfAvailableBook(isbn);
  }
  
  public void addSpecificNumberOfBookInstance(long isbn, java.lang.String author, java.lang.String title, java.lang.String description, java.lang.String category, double price, int numberToAdd, java.lang.String[] keywords) throws java.rmi.RemoteException{
    if (bibliothekManagerImpl == null)
      _initBibliothekManagerImplProxy();
    bibliothekManagerImpl.addSpecificNumberOfBookInstance(isbn, author, title, description, category, price, numberToAdd, keywords);
  }
  
  public fr.umlv.bibliothek.service.Book removeSpecificNumberOfBookInstanceByISBN(long isbn, int numberToRemove) throws java.rmi.RemoteException{
    if (bibliothekManagerImpl == null)
      _initBibliothekManagerImplProxy();
    return bibliothekManagerImpl.removeSpecificNumberOfBookInstanceByISBN(isbn, numberToRemove);
  }
  
  
}