package fr.umlv.Shopping;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.rpc.ServiceException;

import DefaultNamespace.BankManager;
import DefaultNamespace.BankManagerSoapBindingStub;
import DefaultNamespace.Converter;
import DefaultNamespace.ConverterServiceLocator;
import fr.umlv.User.UserManager;
import fr.umlv.User.UserManagerProxy;
import fr.umlv.bibliothek.BibliothekManagerImpl;
import fr.umlv.bibliothek.BibliothekManagerImplServiceLocator;
import fr.umlv.bibliothek.service.Book;

public class Shopping implements Serializable{

	private BibliothekManagerImpl bib;
	private BankManager bank;
	private Converter converter;
	private ShoppingCart shopcart;
	private String currency = "EUR";
	private UserManager um;
	private String loggin = "";
	private String mdp = "";

	/**
	 * Constructor.
	 */
	public Shopping() {

		try {
			bib = new BibliothekManagerImplServiceLocator().getBibliothekManagerImpl();
			
			bank = new DefaultNamespace.BankManagerServiceLocator()
					.getBankManager();
			((BankManagerSoapBindingStub)bank).setMaintainSession(true);
			shopcart = new ShoppingCart();
			this.converter = new ConverterServiceLocator().getConverter();
			this.um = new UserManagerProxy().getUserManager();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}

	public BibliothekManagerImpl getBib() throws RemoteException {
		return bib;
	}

	public BankManager getBank() {
		return bank;
	}

	public ShoppingCart getShopcart() {
		return shopcart;
	}

	public UserManager getUm() {
		return um;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency_dest) {
		this.currency = currency_dest;
	}

	/**
	 * Search all book from the specified author.
	 * 
	 * @param author
	 * @return
	 * @throws RemoteException
	 */
	public Book[] searchBookByAuthor(String author) throws RemoteException {
		return bib.searchBookByAuthor(author);
	}
	
	public fr.umlv.Shopping.service.Book[] searchRemoteBookByAuthor(String author) throws RemoteException {
		Book[] allbooks = bib.searchBookByAuthor(author);
		List<Book> books = Arrays.asList(allbooks);
		List<fr.umlv.Shopping.service.Book> bookList = new ArrayList<>();
		for(Book b : books){
			bookList.add(new fr.umlv.Shopping.service.Book(b));
		}	
		
		return bookList.toArray(new fr.umlv.Shopping.service.Book[bookList.size()]);	
	}

	/**
	 * 
	 * @param isbn
	 * @return
	 * @throws RemoteException
	 */
	public Book removeBookByISBN(int isbn) throws RemoteException {
		return bib.removeBookByISBN(isbn);
	}
	
	public fr.umlv.Shopping.service.Book removeRemoteBookByISBN(long isbn) throws RemoteException {
		return new fr.umlv.Shopping.service.Book(bib.removeBookByISBN(isbn));
	}

	/**
	 * Search all book corresponding containing the specified title.
	 * 
	 * @param title
	 * @return
	 * @throws RemoteException
	 */
	public Book[] searchBookByTitle(String title) throws RemoteException {
		return bib.searchBookByTitle(title);
	}
	
	public fr.umlv.Shopping.service.Book[] searchRemoteBookByTitle(String title) throws RemoteException {
		
		Book[] allbooks = bib.searchBookByTitle(title);
		List<Book> books = Arrays.asList(allbooks);
		List<fr.umlv.Shopping.service.Book> bookList = new ArrayList<>();
		for(Book b : books){
			bookList.add(new fr.umlv.Shopping.service.Book(b));
		}	

		return bookList.toArray(new fr.umlv.Shopping.service.Book[bookList.size()]);	
	}

	/**
	 * Search all book corresponding with the specified price.
	 * 
	 * @param price
	 * @return
	 * @throws RemoteException
	 */
	public Book[] searchBookByPrice(double price) throws RemoteException {
		return bib.searchBookByPrice(price);
	}
	
	public fr.umlv.Shopping.service.Book[] searchRemoteBookByPrice(double price) throws RemoteException {
		Book[] allbooks = bib.searchBookByPrice(price);
		List<Book> books = Arrays.asList(allbooks);
		List<fr.umlv.Shopping.service.Book> bookList = new ArrayList<>();
		for(Book b : books){
			bookList.add(new fr.umlv.Shopping.service.Book(b));
		}		
		return bookList.toArray(new fr.umlv.Shopping.service.Book[bookList.size()]);
		
	}

	/**
	 * Search all book corresponding with the specified key words.
	 * 
	 * @param keyWord
	 * @return
	 * @throws RemoteException
	 */
	public Book[] searchBookByKeyWords(String keyWord) throws RemoteException {
		return bib.searchBookByKeyWords(keyWord);
	}
	
	public fr.umlv.Shopping.service.Book[] searchRemoteBookByKeyWords(String keyWord) throws RemoteException {
		Book[] allbooks =bib.searchBookByKeyWords(keyWord);
		List<Book> books = Arrays.asList(allbooks);
		List<fr.umlv.Shopping.service.Book> bookList = new ArrayList<>();
		for(Book b : books){
			bookList.add(new fr.umlv.Shopping.service.Book(b));
		}		
		return bookList.toArray(new fr.umlv.Shopping.service.Book[bookList.size()]);
		
		
	}

	/**
	 * Search all book corresponding with the specified category.
	 * 
	 * @param category
	 * @return
	 * @throws RemoteException
	 */
	public Book[] searchBookByCategory(String category) throws RemoteException {
		return bib.searchBookByCategory(category);
	}
	
	public fr.umlv.Shopping.service.Book[] searchRemoteBookByCategory(String category) throws RemoteException {
		Book[] allbooks =bib.searchBookByCategory(category);
		List<Book> books = Arrays.asList(allbooks);
		List<fr.umlv.Shopping.service.Book> bookList = new ArrayList<>();
		for(Book b : books){
			bookList.add(new fr.umlv.Shopping.service.Book(b));
		}		
		return bookList.toArray(new fr.umlv.Shopping.service.Book[bookList.size()]);
		
	}

	/**
	 * Add a book to the bibliothek
	 * 
	 * @param isbn
	 * @param author
	 * @param title
	 * @param description
	 * @param category
	 * @param price
	 * @param keywords
	 * @param num_account
	 * @param name
	 * @param firstname
	 * @param currency
	 * @return
	 * @throws RemoteException
	 */
	public void addBook(Book b) throws RemoteException {
		bib.addBook(b.getISBN(), b.getAutor(), b.getTitle(),
				b.getDescription(), b.getCategory(), b.getPrice(), null);
		;
	}
	
	public void addRemoteBook(fr.umlv.Shopping.service.Book b) throws RemoteException {
		bib.addBook(b.getISBN(), b.getAutor(), b.getTitle(),
				b.getDescription(), b.getCategory(), b.getPrice(), null);
		
	}
	
	public boolean addRemoteBookByISBN(long isbn) throws RemoteException {
		return addBookIntoCart(bib.getBookByISBN(isbn));		
	}

	/**
	 * Sell a book, credit the user account.
	 * 
	 * @param b
	 * @param num_account
	 * @param name
	 * @param firstname
	 * @param currency
	 * @return
	 * @throws RemoteException
	 */
	public boolean sellBook(Book b, long num_account, String name,
			String firstname, String currency) throws RemoteException {
		return bank.deposit(num_account, name, firstname, currency,
				b.getPrice());
	}
	
	public boolean sellRemoteBook(fr.umlv.Shopping.service.Book b, long num_account, String name,
			String firstname, String currency) throws RemoteException {
		return bank.deposit(num_account, name, firstname, currency,
				b.getPrice());
	}

	/**
	 * Get all books from the servers.
	 * 
	 * @return
	 * @throws RemoteException
	 */
	public Book[] getAllBook() throws RemoteException {
		return bib.getAllBooks();
	}
	
	public fr.umlv.Shopping.service.Book[] getAllRemoteBook() throws RemoteException {
		Book[] allBooks = bib.getAllBooks();
		List<Book> books = Arrays.asList(allBooks);
		List<fr.umlv.Shopping.service.Book> bookList = new ArrayList<>();
		for(Book b : books){
			bookList.add(new fr.umlv.Shopping.service.Book(b));
		}		
		return bookList.toArray(new fr.umlv.Shopping.service.Book[bookList.size()]);
	}

	/**
	 * Get all books in bibliotheque filtered by category.s
	 * 
	 * @param category
	 * @return
	 * @throws RemoteException
	 */
	public Book[] getAllBookByCategory(String category) throws RemoteException {
		return bib.getAllBooksByCategory(category);

	}
	
	public fr.umlv.Shopping.service.Book[] getAllRemoteBookByCategory(String category) throws RemoteException {
		Book[] allBooksByCategory = bib.getAllBooksByCategory(category);
		List<Book> books = Arrays.asList(allBooksByCategory);
		List<fr.umlv.Shopping.service.Book> bookList = new ArrayList<>();
		for(Book b : books){
			bookList.add(new fr.umlv.Shopping.service.Book(b));
		}		
		return bookList.toArray(new fr.umlv.Shopping.service.Book[bookList.size()]);

	}

	/**
	 * Get a book by his number of identification.
	 * 
	 * @param isbn
	 * @return
	 * @throws RemoteException
	 */
	public Book getBookByIsbn(long isbn) throws RemoteException {
		return bib.getBookByISBN(isbn);
	}
	
	public fr.umlv.Shopping.service.Book getRemoteBookByIsbn(long isbn) throws RemoteException {
		Book book = bib.getBookByISBN(isbn);
		return new fr.umlv.Shopping.service.Book(book);
	}

	/**
	 * Get the number of remaining books.
	 * 
	 * @param isbn
	 * @return
	 * @throws RemoteException
	 */
	public int getNumberOfAvailableBook(Book b) throws RemoteException {
		return bib.getNumberOfAvailableBook(b.getISBN());
	}
	
	public int getNumberOfAvailableRemoteBook(fr.umlv.Shopping.service.Book b) throws RemoteException {
		System.out.println("========> "+b.getISBN());
		return bib.getNumberOfAvailableBook(b.getISBN());
	}
	
	public int getNumberOfAvailableRemoteBookISBN(long isbn) throws RemoteException {
		return bib.getNumberOfAvailableBook(isbn);
	}

	/**
	 * Add book into the shopping cart
	 * 
	 * @param isbn
	 * @param author
	 * @param title
	 * @param description
	 * @param category
	 * @param price
	 * @return true if the book is available in the bibliothek, false either
	 * @throws RemoteException
	 */
	public boolean addBookIntoCart(Book b) throws RemoteException {
		if (bib.isBookAvailable(b.getISBN())) {
			bib.removeBookByISBN(b.getISBN());
			shopcart.setTotalPrice(shopcart.getTotalPrice() + b.getPrice());
			shopcart.setBooknumber(shopcart.getBooknumber() + 1);
			return shopcart.getCart().add(b);
		}
		return false;
	}
	
	
	
	public boolean addBookIntoRemoetCart(fr.umlv.Shopping.service.Book b)
			throws RemoteException {
		return addBookIntoCart(new Book(b.getISBN(), b.getAutor(),
				b.getCategory(), b.getDescription(), b.getPrice(), b.getTitle()));
	}
	
	public boolean addBookIntoRemoteCartByISBN(long isbn)
			throws RemoteException {
		
		return addBookIntoCart(bib.getBookByISBN(isbn));
	}

	/**
	 * Remove book from the shopping cart
	 * 
	 * @param isbn
	 * @param author
	 * @param title
	 * @param description
	 * @param category
	 * @param price
	 * @param keywords
	 * @return true if the book was removed, false either.
	 * @throws RemoteException
	 */
	public boolean removeBookFromCart(Book b) throws RemoteException {
		bib.addBook(b.getISBN(), b.getAutor(), b.getTitle(),
				b.getDescription(), b.getCategory(), b.getPrice(), null);
		shopcart.setTotalPrice(shopcart.getTotalPrice() - b.getPrice());
		shopcart.setBooknumber(shopcart.getBooknumber() - 1);
		return shopcart.getCart().remove(b);
	}

	public boolean removeBookFromCartWithRemoteBook(fr.umlv.Shopping.service.Book b)
			throws RemoteException {

		return removeBookFromCart(new Book(b.getISBN(), b.getAutor(),
				b.getCategory(), b.getDescription(), b.getPrice(), b.getTitle()));
	}
	
	public boolean removeBookFromCartWithRemoteBookByISBN(long isbn)
			throws RemoteException {

		return removeBookFromCart(bib.getBookByISBN(isbn));
	}

	/**
	 * Create a user account.
	 * 
	 * @param login
	 * @param password
	 * @param firstname
	 * @param name
	 * @param email
	 * @param birthDate
	 * @param address
	 * @throws RemoteException
	 */
	public void createUserAccount(String login, String password,
			String firstname, String name, String email, String birthDate,
			String address) throws RemoteException {
		um.userRegistration(login, password, firstname, name, email, birthDate,
				address);
	}

	public Boolean tryToConnect(String login, String password)
			throws RemoteException {
		return um.userConnection(login, password);
	}

	public Boolean tryToDeconnect(String login, String password)
			throws RemoteException {
		return um.userDeconnection(login, password);
	}

	/**
	 * Buy the shopping cart with the bank detail of the user.
	 * 
	 * @return True if the user buy the cart, false either.
	 * @throws RemoteException
	 */
	public boolean buyCart(long num_account, String login, String password,
			String firstname, String name) throws RemoteException {

		// Check if the user already exist and is connected.
		if (!um.userConnection(login, password)) {
			System.out
					.println("User is not connected or user is not registered");
			return false;
		}
		if (!bank.withdrawal(num_account, name, firstname, currency,
				shopcart.getTotalPrice())) {
			System.out.println("Not enought balance for buying");
			return false;
		}
		// supprime tous les livres du panier
		shopcart.getCart().clear();
		shopcart.setBooknumber(shopcart.getCart().size());

		// remet le prix du panier à zero.
		shopcart.setTotalPrice(0);

		return true;
	}

	public double convertMoney(double amount, String from, String to)
			throws RemoteException {
		return this.converter.convert(amount, from, to);
	}

	public String getMdp() throws RemoteException {
		return mdp;
	}

	public String getLoggin() throws RemoteException {
		return loggin;
	}

	public void setMdp(String mdp) throws RemoteException {
		this.mdp = mdp;
	}

	public void setLoggin(String loggin) throws RemoteException {
		this.loggin = loggin;
	}
}
