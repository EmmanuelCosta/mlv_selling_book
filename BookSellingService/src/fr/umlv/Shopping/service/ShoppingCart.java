package fr.umlv.Shopping.service;

import java.util.Arrays;
import java.util.List;



public class ShoppingCart {
	private fr.umlv.Shopping.ShoppingCart shoppingCartService;

	public ShoppingCart() {

	}

	public ShoppingCart(fr.umlv.Shopping.ShoppingCart shoppingCardService) {
		this.shoppingCartService = shoppingCardService;
	}

	public double getTotalPrice() {
		return shoppingCartService.getTotalPrice();
	}

	public Book[] getCart() {
		List<Book> cart = shoppingCartService.getRemoteCart();
		return cart.toArray(new Book[cart.size()]);
	}

	public void setTotalPrice(double totalPrice) {
		shoppingCartService.setTotalPrice(totalPrice);
	}

	public void setBooknumber(long booknumber) {
		shoppingCartService.setBooknumber(booknumber);
	}

	public void setCart(Book[] cart) {
		List<Book> asList = Arrays.asList(cart);
		shoppingCartService.setCartWithLocalBook(asList);
	}

	public long getBooknumber() {
		return shoppingCartService.getBooknumber();
	}
	
	public void clearTheCart(){
		shoppingCartService.clear();
	}

//	public int hashCode() {
//		return shoppingCartService.hashCode();
//	}

//	public String toString() {
//		return shoppingCartService.toString();
//	}
	
	
}
