package fr.umlv.Shopping.service;

import java.rmi.RemoteException;

import fr.umlv.bibliothek.BibliothekManagerImpl;
import fr.umlv.bibliothek.BibliothekManagerImplProxy;
import fr.umlv.bibliothek.service.Book;

public class BibliothekManagerImplement {
	
	private BibliothekManagerImpl bibliothekManagerImpl;
	
	
	
	public BibliothekManagerImplement() {
		bibliothekManagerImpl=new BibliothekManagerImplProxy().getBibliothekManagerImpl();
	}



	public String[] getCategories() throws RemoteException {
		return bibliothekManagerImpl.getCategories();
	}



	public boolean isBookAvailable(long isbn) throws RemoteException {
		return bibliothekManagerImpl.isBookAvailable(isbn);
	}



	public void addBook(long isbn, String author, String title,
			String description, String category, double price, String[] keywords)
			throws RemoteException {
		bibliothekManagerImpl.addBook(isbn, author, title, description,
				category, price, keywords);
	}



	public Book getBookByISBN(long isbn) throws RemoteException {
		return bibliothekManagerImpl.getBookByISBN(isbn);
	}



	public Book removeBookByISBN(long isbn) throws RemoteException {
		return bibliothekManagerImpl.removeBookByISBN(isbn);
	}



	public Book[] getAllBooks() throws RemoteException {
		return bibliothekManagerImpl.getAllBooks();
	}



	public Book[] searchBookByKeyWords(String keyWord) throws RemoteException {
		return bibliothekManagerImpl.searchBookByKeyWords(keyWord);
	}



	public Book[] getAllBooksByCategory(String category) throws RemoteException {
		return bibliothekManagerImpl.getAllBooksByCategory(category);
	}



	public Book[] searchBookByAuthor(String author) throws RemoteException {
		return bibliothekManagerImpl.searchBookByAuthor(author);
	}



	public Book[] searchBookByTitle(String title) throws RemoteException {
		return bibliothekManagerImpl.searchBookByTitle(title);
	}



	public Book[] searchBookByPrice(double price) throws RemoteException {
		return bibliothekManagerImpl.searchBookByPrice(price);
	}



	public Book[] searchBookByCategory(String category) throws RemoteException {
		return bibliothekManagerImpl.searchBookByCategory(category);
	}



	public int getNumberOfAvailableBook(long isbn) throws RemoteException {
		return bibliothekManagerImpl.getNumberOfAvailableBook(isbn);
	}



	public void addSpecificNumberOfBookInstance(long isbn, String author,
			String title, String description, String category, double price,
			int numberToAdd, String[] keywords) throws RemoteException {
		bibliothekManagerImpl.addSpecificNumberOfBookInstance(isbn, author,
				title, description, category, price, numberToAdd, keywords);
	}



	public Book removeSpecificNumberOfBookInstanceByISBN(long isbn,
			int numberToRemove) throws RemoteException {
		return bibliothekManagerImpl.removeSpecificNumberOfBookInstanceByISBN(
				isbn, numberToRemove);
	}
	
}
