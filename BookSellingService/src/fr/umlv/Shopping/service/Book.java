package fr.umlv.Shopping.service;

import java.io.Serializable;

public class Book implements Serializable{
	private fr.umlv.bibliothek.service.Book book;

	public Book() {
	}

	public Book(fr.umlv.bibliothek.service.Book book) {
		this.book = book;
	}

	public Long getISBN() {
		return book.getISBN();
	}

//	public void setISBN(long ISBN) {
//		book.setISBN(ISBN);
//	}

	public String getAutor() {
		return book.getAutor();
	}

//	public void setAutor(String autor) {
//		book.setAutor(autor);
//	}

	public String getCategory() {
		return book.getCategory();
	}

//	public void setCategory(String category) {
//		book.setCategory(category);
//	}

	public String getDescription() {
		return book.getDescription();
	}

//	public void setDescription(String description) {
//		book.setDescription(description);
//	}

	public double getPrice() {
		return book.getPrice();
	}

//	public void setPrice(double price) {
//		book.setPrice(price);
//	}

	public String getTitle() {
		return book.getTitle();
	}

//	public void setTitle(String title) {
//		book.setTitle(title);
//	}

//	public boolean equals(Object obj) {
//		return book.equals(obj);
//	}
//
//	public int hashCode() {
//		return book.hashCode();
//	}
//
//	public String toString() {
//		return book.toString();
//	}	
}
