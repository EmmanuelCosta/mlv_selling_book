package fr.umlv.Shopping.service;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.Objects;



public class Shopping implements Serializable {

	private fr.umlv.Shopping.Shopping shopping;

	

	public Shopping() {
		this.shopping = new fr.umlv.Shopping.Shopping();
	}

	public BankManager getBank() {
		return new BankManager(shopping.getBank());
	}

	public ShoppingCart getShopcart() {
		return new ShoppingCart(shopping.getShopcart());
	}

	public UserManager getUm() {
		return new UserManager(shopping.getUm());
	}

	public String getCurrency() {
		return shopping.getCurrency();
	}

	

	public void setCurrency(String currency_dest) {
		shopping.setCurrency(currency_dest);
	}

	public Book[] searchBookByAuthor(String author) throws RemoteException {
		return shopping.searchRemoteBookByAuthor(author);
	}

	public Book removeBookByISBN(long isbn) throws RemoteException {
		return shopping.removeRemoteBookByISBN(isbn);
	}

	public Book[] searchBookByTitle(String title) throws RemoteException {
		return shopping.searchRemoteBookByTitle(title);
	}

	public Book[] searchBookByPrice(double price) throws RemoteException {
		return shopping.searchRemoteBookByPrice(price);
	}

	public Book[] searchBookByKeyWords(String keyWord) throws RemoteException {
		return shopping.searchRemoteBookByKeyWords(keyWord);
	}

	public Book[] searchBookByCategory(String category) throws RemoteException {
		return shopping.searchRemoteBookByCategory(category);
	}

	
	
	public void addBookByISBN(long isbn) throws RemoteException {
		shopping.addRemoteBookByISBN(isbn);
	}

	public boolean sellBook(Book b, long num_account, String name,
			String firstname, String currency) throws RemoteException {
		return shopping.sellRemoteBook(b, num_account, name, firstname, currency);
	}
	
	

	public Book[] getAllBook() throws RemoteException {
		return shopping.getAllRemoteBook();
	}

	public Book[] getAllBookByCategory(String category) throws RemoteException {
		return shopping.getAllRemoteBookByCategory(category);
	}

	public Book getBookByIsbn(long isbn) throws RemoteException {
		return shopping.getRemoteBookByIsbn(isbn);
	}

	
	
	public int getNumberOfAvailableByBISBNBook(long isbn) throws RemoteException {
		return shopping.getNumberOfAvailableRemoteBookISBN(isbn);
	}

	
	
	public boolean addBookIntoCartByISBN(long isbn) throws RemoteException {
		boolean result=shopping.addBookIntoRemoteCartByISBN(isbn);
		return result;
	}

	
	
	public boolean removeBookFromCartByISBN(long isbn) throws RemoteException {
		return shopping.removeBookFromCartWithRemoteBookByISBN(isbn);
	}

	public void createUserAccount(String login, String password,
			String firstname, String name, String email, String birthDate,
			String address) throws RemoteException {
		shopping.createUserAccount(login, password, firstname, name, email,
				birthDate, address);
	}

	public Boolean tryToConnect(String login, String password)
			throws RemoteException {
		return shopping.tryToConnect(login, password);
	}

	public Boolean tryToDeconnect(String login, String password)
			throws RemoteException {
		return shopping.tryToDeconnect(login, password);
	}

	public boolean buyCart(long num_account, String login, String password,
			String firstname, String name) throws RemoteException {
		return shopping.buyCart(num_account, login, password, firstname, name);
	}

	public double convertMoney(double amount, String from, String to)
			throws RemoteException {
		return shopping.convertMoney(amount, from, to);
	}

	public String getMdp() throws RemoteException {
		return shopping.getMdp();
	}

	public String getLoggin() throws RemoteException {
		return shopping.getLoggin();
	}

	public void setMdp(String mdp) throws RemoteException {
		shopping.setMdp(mdp);
	}

	public void setLoggin(String loggin) throws RemoteException {
		shopping.setLoggin(loggin);
	}

	public void clearTheCart(){
		 shopping.getShopcart().clear();
	}

}
