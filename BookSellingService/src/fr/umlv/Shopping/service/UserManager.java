package fr.umlv.Shopping.service;

import java.rmi.RemoteException;

public class UserManager {
	private fr.umlv.User.UserManager userManager;

	public UserManager() {
		// TODO Auto-generated constructor stub
	}

	public UserManager(fr.umlv.User.UserManager userManager) {
		this.userManager = userManager;
	}

	public boolean userConnection(String login, String password)
			throws RemoteException {
		return userManager.userConnection(login, password);
	}

	public boolean userDeconnection(String login, String password)
			throws RemoteException {
		return userManager.userDeconnection(login, password);
	}

	public void userRegistration(String login, String password,
			String firstname, String name, String email, String birthDate,
			String address) throws RemoteException {
		userManager.userRegistration(login, password, firstname, name, email,
				birthDate, address);
	}

}
