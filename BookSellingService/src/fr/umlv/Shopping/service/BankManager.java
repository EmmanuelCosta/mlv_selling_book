package fr.umlv.Shopping.service;

import java.rmi.RemoteException;

public class BankManager {

	private DefaultNamespace.BankManager bankManager;

	public BankManager() {

	}

	public BankManager(DefaultNamespace.BankManager bankManager) {
		this.bankManager = bankManager;
	}

	public boolean withdrawal(long num_account, String name, String firstname,
			String currency, double amount) throws RemoteException {
		return bankManager.withdrawal(num_account, name, firstname, currency,
				amount);
	}

	public boolean deposit(long num_account, String name, String firstname,
			String currency, double amount) throws RemoteException {
		return bankManager.deposit(num_account, name, firstname, currency,
				amount);
	}

}
