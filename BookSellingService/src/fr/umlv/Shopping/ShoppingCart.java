package fr.umlv.Shopping;

import java.util.ArrayList;
import java.util.List;

import fr.umlv.bibliothek.service.Book;

/**
 * @author sybille
 *
 */
public class ShoppingCart {

	private double totalPrice;
	private long booknumber;
	private List<fr.umlv.bibliothek.service.Book> cart;

	/**
	 * Constructor
	 */
	public ShoppingCart() {
		this.booknumber = 0;
		this.totalPrice = 0;
		this.cart = new ArrayList<fr.umlv.bibliothek.service.Book>();
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public List<fr.umlv.bibliothek.service.Book> getCart() {
		return cart;
	}
	
	public List<fr.umlv.Shopping.service.Book> getRemoteCart() {
		List<fr.umlv.Shopping.service.Book> books = new ArrayList<>();
		for(Book b : this.cart){
			books.add(new fr.umlv.Shopping.service.Book(b));
		}
		return books;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public void setBooknumber(long booknumber) {
		this.booknumber = booknumber;
	}

	public void setCart(List<fr.umlv.bibliothek.service.Book> cart) {

		this.cart.addAll(cart);
	}

	public void setCartWithLocalBook(List<fr.umlv.Shopping.service.Book> cart) {
		
		for(fr.umlv.Shopping.service.Book b : cart){
			this.cart.add(new Book(b.getISBN(), b.getAutor(), b.getCategory(), b.getDescription(), b.getPrice(), b.getTitle()));
		}
		//this.cart = cart;
	}

	public long getBooknumber() {
		return cart.size();
	}
	
	public void clear() {
		 cart.clear();
	}

}
