package fr.umlv.Shopping;

import java.rmi.RemoteException;

import fr.umlv.bibliothek.service.Book;

public class Main {
	public static void main (String[] args) throws RemoteException {
			Shopping shop = new Shopping();
			Book b = shop.getBookByIsbn(20L);
			System.out.println("---- LIST OF BOOKS ----\n");
			Book[] bookList = shop.getAllBook();
			for (Book book : bookList) {
				System.out.println(book.getTitle() + " " + book.getPrice() + " " + shop.getNumberOfAvailableBook(book));
			}
			
			shop.addBookIntoCart(b);

			System.out.println("---- UPDATED LIST OF BOOKS ----\n");
			for (Book book : bookList) {
				System.out.println(book.getTitle() + " " + book.getPrice() + " " + shop.getNumberOfAvailableBook(book));
			}
			
			System.out.println("\n---- LIST OF BOOKS IN CART ----\n");
			for (Book book : shop.getShopcart().getCart()) {
				System.out.println(book.getTitle() + " " + book.getPrice() + " " + shop.getNumberOfAvailableBook(book));
			}
			System.out.println("Total price of the shopping cart : " + shop.getShopcart().getTotalPrice());
			System.out.println("Number of books in the shopping cart : " + shop.getShopcart().getBooknumber());
			
			System.out.println("\n---- TRY TO BUY CART ----\n");
			boolean isbought = shop.buyCart(19581433, "scrimet", "feicooW4", "Sybille", "Crimet");
			if(isbought)
				System.out.println("YOU HAVE BOUGHT YOUR SHOPPING CART");
			else {
				System.out.println("YOU CAN'T BUY YOUR SHOPPING CART FOR THE FOLLOWING REASONS : ");
				System.out.println(" == > you are not connected ");
				System.out.println(" == > you don't have enought money");
			}
			System.out.println("Total price of shooping cart after buy : " + shop.getShopcart().getTotalPrice());
			System.out.println("Total number of books in the shooping cart after buy : " + shop.getShopcart().getBooknumber());
			
			System.out.println("\n---- CREATE USER ACCOUNT ----\n");
			shop.createUserAccount("onorture", "fSay4aih", "Olivier", "Norture",
					"norture.olivier@gmail.com", "24/06/1991",
					"3 avenue du Maréchal Foch 77680 Roissy-en-Brie FRANCE");
			shop.createUserAccount("ebabalac", "toto5", "Emmanuel", "Ebabala-Costa", "ebabala@etudiant.univ-mlv.fr", "25/12/1990", "2 rue du pavot 77160 Chelles");
			
			System.out.println("Try manu to buy the shooping cart: " + shop.getShopcart().getTotalPrice());
			System.out.println(shop.buyCart(19561433, "ebabalac", "toto5", "Emmanuel", "Ebabala-Costa"));
	}
}