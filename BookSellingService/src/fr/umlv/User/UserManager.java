/**
 * UserManager.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package fr.umlv.User;

public interface UserManager extends java.rmi.Remote {
    public boolean userConnection(java.lang.String login, java.lang.String password) throws java.rmi.RemoteException;
    public boolean userDeconnection(java.lang.String login, java.lang.String password) throws java.rmi.RemoteException;
    public void userRegistration(java.lang.String login, java.lang.String password, java.lang.String firstname, java.lang.String name, java.lang.String email, java.lang.String birthDate, java.lang.String address) throws java.rmi.RemoteException;
}
