package fr.umlv.User;

public class UserManagerProxy implements fr.umlv.User.UserManager {
  private String _endpoint = null;
  private fr.umlv.User.UserManager userManager = null;
  
  public UserManagerProxy() {
    _initUserManagerProxy();
  }
  
  public UserManagerProxy(String endpoint) {
    _endpoint = endpoint;
    _initUserManagerProxy();
  }
  
  private void _initUserManagerProxy() {
    try {
      userManager = (new fr.umlv.User.UserManagerServiceLocator()).getUserManager();
      if (userManager != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)userManager)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)userManager)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (userManager != null)
      ((javax.xml.rpc.Stub)userManager)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public fr.umlv.User.UserManager getUserManager() {
    if (userManager == null)
      _initUserManagerProxy();
    return userManager;
  }
  
  public boolean userConnection(java.lang.String login, java.lang.String password) throws java.rmi.RemoteException{
    if (userManager == null)
      _initUserManagerProxy();
    return userManager.userConnection(login, password);
  }
  
  public boolean userDeconnection(java.lang.String login, java.lang.String password) throws java.rmi.RemoteException{
    if (userManager == null)
      _initUserManagerProxy();
    return userManager.userDeconnection(login, password);
  }
  
  public void userRegistration(java.lang.String login, java.lang.String password, java.lang.String firstname, java.lang.String name, java.lang.String email, java.lang.String birthDate, java.lang.String address) throws java.rmi.RemoteException{
    if (userManager == null)
      _initUserManagerProxy();
    userManager.userRegistration(login, password, firstname, name, email, birthDate, address);
  }
  
  
}