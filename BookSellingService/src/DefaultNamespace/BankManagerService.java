/**
 * BankManagerService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package DefaultNamespace;

public interface BankManagerService extends javax.xml.rpc.Service {
    public java.lang.String getBankManagerAddress();

    public DefaultNamespace.BankManager getBankManager() throws javax.xml.rpc.ServiceException;

    public DefaultNamespace.BankManager getBankManager(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
