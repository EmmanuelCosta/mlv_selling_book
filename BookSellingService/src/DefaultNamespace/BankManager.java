/**
 * BankManager.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package DefaultNamespace;

public interface BankManager extends java.rmi.Remote {
    public boolean withdrawal(long num_account, java.lang.String name, java.lang.String firstname, java.lang.String currency, double amount) throws java.rmi.RemoteException;
    public boolean deposit(long num_account, java.lang.String name, java.lang.String firstname, java.lang.String currency, double amount) throws java.rmi.RemoteException;
}
