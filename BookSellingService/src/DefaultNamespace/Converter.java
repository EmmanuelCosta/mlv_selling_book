/**
 * Converter.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package DefaultNamespace;

public interface Converter extends java.rmi.Remote {
    public double convert(double val, java.lang.String currency, java.lang.String currency_dest) throws java.rmi.RemoteException;
    public double rate(java.lang.String currency, java.lang.String currency_dest) throws java.rmi.RemoteException;
}
