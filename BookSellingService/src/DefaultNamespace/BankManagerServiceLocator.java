/**
 * BankManagerServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package DefaultNamespace;

public class BankManagerServiceLocator extends org.apache.axis.client.Service implements DefaultNamespace.BankManagerService {

    public BankManagerServiceLocator() {
    }


    public BankManagerServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public BankManagerServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for BankManager
    private java.lang.String BankManager_address = "http://localhost:8080/BankService/services/BankManager";

    public java.lang.String getBankManagerAddress() {
        return BankManager_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String BankManagerWSDDServiceName = "BankManager";

    public java.lang.String getBankManagerWSDDServiceName() {
        return BankManagerWSDDServiceName;
    }

    public void setBankManagerWSDDServiceName(java.lang.String name) {
        BankManagerWSDDServiceName = name;
    }

    public DefaultNamespace.BankManager getBankManager() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(BankManager_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getBankManager(endpoint);
    }

    public DefaultNamespace.BankManager getBankManager(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            DefaultNamespace.BankManagerSoapBindingStub _stub = new DefaultNamespace.BankManagerSoapBindingStub(portAddress, this);
            _stub.setPortName(getBankManagerWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setBankManagerEndpointAddress(java.lang.String address) {
        BankManager_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (DefaultNamespace.BankManager.class.isAssignableFrom(serviceEndpointInterface)) {
                DefaultNamespace.BankManagerSoapBindingStub _stub = new DefaultNamespace.BankManagerSoapBindingStub(new java.net.URL(BankManager_address), this);
                _stub.setPortName(getBankManagerWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("BankManager".equals(inputPortName)) {
            return getBankManager();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://DefaultNamespace", "BankManagerService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://DefaultNamespace", "BankManager"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("BankManager".equals(portName)) {
            setBankManagerEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
