package DefaultNamespace;

public class BankManagerProxy implements DefaultNamespace.BankManager {
  private String _endpoint = null;
  private DefaultNamespace.BankManager bankManager = null;
  
  public BankManagerProxy() {
    _initBankManagerProxy();
  }
  
  public BankManagerProxy(String endpoint) {
    _endpoint = endpoint;
    _initBankManagerProxy();
  }
  
  private void _initBankManagerProxy() {
    try {
      bankManager = (new DefaultNamespace.BankManagerServiceLocator()).getBankManager();
      if (bankManager != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bankManager)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bankManager)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bankManager != null)
      ((javax.xml.rpc.Stub)bankManager)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public DefaultNamespace.BankManager getBankManager() {
    if (bankManager == null)
      _initBankManagerProxy();
    return bankManager;
  }
  
  public boolean withdrawal(long num_account, java.lang.String name, java.lang.String firstname, java.lang.String currency, double amount) throws java.rmi.RemoteException{
    if (bankManager == null)
      _initBankManagerProxy();
    return bankManager.withdrawal(num_account, name, firstname, currency, amount);
  }
  
  public boolean deposit(long num_account, java.lang.String name, java.lang.String firstname, java.lang.String currency, double amount) throws java.rmi.RemoteException{
    if (bankManager == null)
      _initBankManagerProxy();
    return bankManager.deposit(num_account, name, firstname, currency, amount);
  }
  
  
}