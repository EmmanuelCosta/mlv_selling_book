package fr.umlv.User;

/**
 * 
 * @author sybille
 * 
 *         User identity paper
 */
public class User {

	private String login;
	private String password;
	private String firstname;
	private String name;
	private String email;
	private String birthDate;
	private String address;
	private Boolean isConnected;

	public User() {

	}

	public User(String login, String password, String firstname, String name,
			String email, String birthDate, String address) {
		super();
		this.login = login;
		this.password = password;
		this.firstname = firstname;
		this.name = name;
		this.email = email;
		this.birthDate = birthDate;
		this.address = address;
	}

	public User(User user) {
		this.login = user.login;
		this.password = user.password;
		this.firstname = user.firstname;
		this.name = user.name;
		this.email = user.email;
		this.birthDate = user.birthDate;
		this.address = user.address;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public boolean isConnected() {
		return isConnected;
	}

	public void setConnection(boolean isConnected) {
		this.isConnected = isConnected;
	}

	@Override
	public String toString() {
		return new StringBuffer().append(login).append(";").append(password)
				.append(";").append(firstname).append(";").append(name)
				.append(";").append(email).append(";").append(birthDate)
				.append(";").append(address).append("\n").toString();
	}
}