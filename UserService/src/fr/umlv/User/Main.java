package fr.umlv.User;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.List;
import org.xml.sax.SAXException;

public class Main {

	/**
	 * Main for test the registration of a user.
	 * 
	 * @param args
	 * @throws SAXException
	 * @throws IOException
	 */
	public static void main(String[] args){
		UserManager um = null;
		boolean isconnect = false;
		try {
			um = new UserManager();
			List<User> list = um.getUsers();

			for (User u : list)
				System.out.print(u.toString());
			um.userRegistration("onorture", "fSay4aih", "Olivier", "Norture",
					"norture.olivier@gmail.com", "24/06/1991",
					"3 avenue du Maréchal Foch 77680 Roissy-en-Brie FRANCE");
			isconnect = um.userConnection("scrimet", "feicooW4");
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(isconnect);
	}
}
