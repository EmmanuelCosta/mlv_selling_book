package fr.umlv.User;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLStreamException;

import org.xml.sax.SAXException;

/**
 * 
 * @author sybille
 * 
 *         Manage customers of website.
 */
public class UserManager {

	private List<User> users;
	private String registry;

	/**
	 * Initialize the list of user from the userRegistry file. Put each user in
	 * a User object and all the User objects are containing in a list of User.
	 * 
	 * @throws FileNotFoundException
	 */
	public UserManager() throws RemoteException {
		users = new ArrayList<User>();
		registry = getClass().getClassLoader().getResource("userRegistry")
				.getFile();
		initUserList();
	}

	/**
	 * Load the list of users from a text file.
	 * 
	 * @throws FileNotFoundException
	 */
	private void initUserList() {
		users.clear();
		BufferedReader file = null;
		try {
			file = new BufferedReader(new FileReader(new File(registry)));
			String line;
			while ((line = file.readLine()) != null) {
				String[] infos = line.split(";");
				User newUser = new User(infos[0], infos[1], infos[2], infos[3],
						infos[4], infos[5], infos[6]);
				users.add(newUser);
			}
			file.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Save the list of users into a text file. The text is overwriting each
	 * time.
	 * 
	 * @param login
	 * @param password
	 * @param firstname
	 * @param name
	 * @param email
	 * @param birthDate
	 * @param street
	 * @param number
	 * @param postcode
	 * @param city
	 * @param country
	 * @throws FileNotFoundException
	 * @throws FactoryConfigurationError
	 * @throws XMLStreamException
	 * @throws SAXException
	 * @throws IOException
	 */
	public void userRegistration(String login, String password,
			String firstname, String name, String email, String birthDate,
			String address) throws RemoteException {

		User newUser = new User(login, password, firstname, name, email,
				birthDate, address);
		if (checkIfExist(newUser))
			return;
		else {
			users.add(newUser);
			try {
				BufferedWriter writer = new BufferedWriter(new FileWriter(
						registry, false));
				for (User u : users)
					writer.write(u.toString());
				writer.close();
				initUserList();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Check if the user have already an account.
	 * 
	 * @param u
	 * @return true if the user has an account, false either.
	 */
	private boolean checkIfExist(User u) {
		for (User user : users) {
			if (user.getLogin().equals(u.getLogin())
					&& user.getPassword().equals(u.getPassword())) {
				System.out.println("The user have already an account");
				return true;
			}
		}
		return false;
	}

	/**
	 * Try to connect a user.
	 * 
	 * @param login
	 * @param password
	 * @return true if login and password are matched, false either.
	 */
	public boolean userConnection(String login, String password)
			throws RemoteException {
		for (User user : users) {
			if (user.getLogin().equals(login)
					&& user.getPassword().equals(password)) {
				user.setConnection(true);
				return true;
			}
		}
		return false;
	}

	/**
	 * Try to deconnect the user.
	 * 
	 * @param login
	 * @param password
	 * @return
	 * @throws RemoteException
	 */
	public boolean userDeconnection(String login, String password)
			throws RemoteException {
		for (User user : users) {
			if (user.getLogin().equals(login)
					&& user.getPassword().equals(password)) {

				user.setConnection(false);
				return true;
			}
		}
		return false;
	}

	List<User> getUsers() {
		return users;
	}

	public static void main(String[] args) {
		// new UserManager().
	}
}
