package DefaultNamespace;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.rpc.ServiceException;

public class BankManager 
{
	ConcurrentHashMap<Long,Account> accounts;
	
	public BankManager()
	{
		accounts=new ConcurrentHashMap<>();
		init();
	}
	
	private void init() 
	{
		ArrayList<String> lines = new ArrayList<>();
		lines.add("10000000 Dupont Pierre USD 100");
		lines.add("10000502 Durant Serge JPY 300");
		lines.add("16000503 Siette Karine EUR 500");
		lines.add("19581433 Crimet Sybille GBP 50");
		lines.add("19561433 Ebabalac-Costa Emmanuel JPY 700500082179");
		
		for(String line: lines)
			{				
				String[] tab=line.split(" ");
				addAccount(Long.parseLong(tab[0]),tab[1],tab[2],tab[3],Double.parseDouble(tab[4]));
			}
			
		
		
	}
	
	private void addAccount(long number, String name, String firstname, String currency, double balance)
	{
		accounts.put(number,new Account(number,name,firstname,currency,balance));
	}
	
	private void deleteAccount(long number)
	{
		accounts.remove(number);
	}
	
	public boolean deposit(long num_account, String name, String firstname, String currency, double amount) throws ServiceException, RemoteException
	{
		Account current=accounts.get(num_account);
		if(current == null || !(current.isValid(num_account, name, firstname)))
			return false;
		if(currency!=current.getCurrency())
		{
			Converter conv=new ConverterServiceLocator().getConverter();
			amount=conv.convert(amount, currency, current.getCurrency());
			currency=current.getCurrency();
		}
		current.deposit(amount);
		return true;
	}
	
	public boolean withdrawal(long num_account, String name, String firstname, String currency, double amount) throws ServiceException, RemoteException
	{
		Account current=accounts.get(num_account);
		if(current == null || !(current.isValid(num_account, name, firstname)))
			return false;
		if(currency!=current.getCurrency())
		{
			Converter conv=new ConverterServiceLocator().getConverter();
			amount=conv.convert(amount, currency, current.getCurrency());
			currency=current.getCurrency();
		}
		return current.withdrawal(amount);
			
	}
	
		
}
