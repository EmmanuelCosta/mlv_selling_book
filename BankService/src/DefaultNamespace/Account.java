package DefaultNamespace;

public class Account
{
	private double balance;
	private long number;
	private String name;
	private String firstname;
	private String currency;
	public Account()
	{
		this.balance=0;
		this.number=0;
		this.name="";
		this.firstname="";
		this.currency="EUR";
	}
	public Account(long number, String name, String firstname, String currency, double balance)
	{
		this.balance=balance;
		this.currency=currency;
		this.name=name;
		this.firstname=firstname;
		this.number=number;
	}

	public String getCurrency(){ return currency;}
	
	public boolean isValid(long number, String name, String firstname)
	{
		return (this.number==number && this.name.equals(name) && this.firstname.equals(firstname));

	}
	public void deposit(double amount)
	{
		balance+=amount;
	}
	public boolean withdrawal(double amount)
	{
		if(authorizedPayment(amount))
		{
			balance-=amount;
			return true;
		}
		else return false;
			
	}
	public boolean authorizedPayment(double amount){return (balance-amount>=0);}
	public double ValueOfBalance(){return balance;}
}
