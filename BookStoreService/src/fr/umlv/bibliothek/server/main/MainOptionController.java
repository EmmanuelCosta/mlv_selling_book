package fr.umlv.bibliothek.server.main;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.Naming;
import java.rmi.RemoteException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import fr.umlv.bibliothek.server.BookStore;
import fr.umlv.bibliothek.server.BookStoreImpl;
import fr.umlv.bibliothek.server.Category.CategoryEnum;

public class MainOptionController {
	public final static String NotDefineArgsValue = "NOT_DEFINE";
	public final static String BADCATEGORY = "BAD_CATEGORY";

	public static String[] retreiveArgs(String[] args) throws ParseException {
		String[] argumentValue = new String[4];
		argumentValue[0] = NotDefineArgsValue;
		argumentValue[1] = NotDefineArgsValue;
		argumentValue[2] = NotDefineArgsValue;
		argumentValue[3] = NotDefineArgsValue;
		Options options = new Options();
		options.addOption("DBOOKSTORENAME", true,
				"Option for the name of the BookStore which will be bind to the server");
		options.addOption("DIPADRESS", true,
				"Option for the ip  of the computer on which run the server");
		options.addOption("DPORT", true,
				"Option for the port use by the rmiregistry");
		options.addOption("DCATEGORY", true,
				"Option for specify with category of book will be manage by this BookStore");
		CommandLineParser cmd = new GnuParser();
		CommandLine parse = cmd.parse(options, args);

		if (parse.hasOption("DBOOKSTORENAME")) {
			String bookStoreName = parse.getOptionValue("DBOOKSTORENAME");
			argumentValue[0] = bookStoreName;
		}

		if (parse.hasOption("DIPADRESS")) {
			String ip = parse.getOptionValue("DIPADRESS");
			argumentValue[1] = ip;
		}
		if (parse.hasOption("DPORT")) {
			String port = parse.getOptionValue("DPORT");
			argumentValue[2] = port;
		}
		if (parse.hasOption("DCATEGORY")) {
			String category = parse.getOptionValue("DCATEGORY");
			try {
				CategoryEnum valueOf = CategoryEnum.valueOf(category);
				argumentValue[3] = category;
			} catch (IllegalArgumentException e) {
				argumentValue[3] = BADCATEGORY;
			}

		}

		return argumentValue;
	}

	public static void proceed(String[] args) throws UnknownHostException,
			ParseException, RemoteException, MalformedURLException {
		String[] retreiveArgs = MainOptionController.retreiveArgs(args);
		String hostAddress = "";
		String bookStoreName = "";
		String port = "";
		CategoryEnum category;

		if (retreiveArgs[3].equals(MainOptionController.NotDefineArgsValue)) {
			System.err.println("INFO :");
			System.err
					.println("You haven't specify a Category for this BookStore\n"
							+ "This bookstore will manage only UNCLASSIFIED Book");
			category = CategoryEnum.UNCLASSIFIED;
		} else if (retreiveArgs[3].equals(MainOptionController.BADCATEGORY)) {
			System.err.println("SEVERE :");
			System.err.println("YOU HAVE SPECIFY A BAD TYPE OF CATEGORY"
					+ "AVAILABLE ARE " + CategoryEnum.ELECTRO + " "
					+ CategoryEnum.HACK + " " + CategoryEnum.PROG_FONC + " "
					+ CategoryEnum.PROG_IMPERATIVE + " "
					+ CategoryEnum.PROG_OBJ + " " + CategoryEnum.WEB + " "
					+ CategoryEnum.UNCLASSIFIED);
			System.err
					.println("Please re run the programm by specify one of the good category");
			return;
		} else {
			category = CategoryEnum.valueOf(retreiveArgs[3]);
		}
		if (retreiveArgs[0].equals(MainOptionController.NotDefineArgsValue)) {
			System.err.println("SEVERE :");
			System.err
					.println("You Must Provide A Name To Be Bind to The Server");
			return;
		} else {
			bookStoreName = retreiveArgs[0];
		}
		if (retreiveArgs[1].equals(MainOptionController.NotDefineArgsValue)) {
			System.err.println("INFO :");
			System.err
					.println("You Must Provide your IP Adress\n"
							+ "We will try to make an auto resolves\n"
							+ "Please notice thar if the server is on virtual Machine your correct ip won't be retreive correctly"
							+ " it may result on error");
			hostAddress = InetAddress.getLocalHost().getHostAddress();
		} else {
			hostAddress = retreiveArgs[1];
		}

		if (retreiveArgs[2].equals(MainOptionController.NotDefineArgsValue)) {
			System.err.println("INFO :");
			System.err
					.println("you haven't specify port. we will run on default port : 1099");
			port = "1099";
		} else {
			port = retreiveArgs[2];
		}

		System.setProperty("java.rmi.server.hostname", hostAddress);

		BookStore bookStore = new BookStoreImpl(category);
		String rmiAdress = "rmi://" + hostAddress + ":" + port + "/"
				+ bookStoreName;
		System.err.println("\nRESUME :");
		System.out.println("The BookStore name= " + bookStoreName
				+ " which manage Book of Category= " + bookStore.getCategory()
				+ " " + " Will connect to= " + hostAddress + "/" + port);
		System.out.println(rmiAdress + "  ");
		Naming.rebind(rmiAdress, bookStore);
	}
}
