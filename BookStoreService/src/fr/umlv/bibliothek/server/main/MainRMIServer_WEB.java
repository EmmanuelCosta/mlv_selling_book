package fr.umlv.bibliothek.server.main;

import java.rmi.Naming;
import java.rmi.RemoteException;

import org.apache.commons.cli.ParseException;

import fr.umlv.bibliothek.server.BookStore;
import fr.umlv.bibliothek.server.BookStoreImpl;
import fr.umlv.bibliothek.server.Category.CategoryEnum;

public class MainRMIServer_WEB {

	public static void main(String[] args) throws RemoteException,
			ParseException {
		try {
			if (args.length == 0) {
				System.err.println("NO OPTION PROVIDED");
				System.err.println("SO APLLY DEFAULT CONF :");
				System.err
						.println("we will launch with this conf : rmi://localhost:1099/BooKSTOREDEFAULT_WEB ");
				System.err
						.println("Note : if you use it in VM trouble can exist better tu use option like this :"
								+ "-DBOOKSTORENAME=XXXX -DCATEGORY=XXXX");
				System.err
						.println("ADDITIONNAL OPTION: -DIPADRESS -DPORT : default: localhost 1099");
				BookStore bookStoreDefault = new BookStoreImpl(CategoryEnum.WEB);
				System.out
						.println("client launch : rmi://localhost:1099/BOOKSTOREDEFAULT_WEB and store only : "
								+ bookStoreDefault.getCategory());
				Naming.rebind("rmi://localhost:1099/BOOKSTOREDEFAULT_WEB",
						bookStoreDefault);
			} else {
				MainOptionController.proceed(args);
			}
		} catch (Exception e) {
			System.out.println("Trouble: " + e);
		}

	}

}
