package fr.umlv.bibliothek.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import fr.umlv.bibliothek.server.Category.CategoryEnum;

public class BookImpl extends UnicastRemoteObject implements Book {
	private Long isbn;
	private String author;
	private String title;
	private String description;
	private Category category;
	private List<String> keywords;
	private double price = 0;

	public BookImpl(Long isbn, String author, String title, String description,
			Category category, String... keywords) throws RemoteException {
		this.isbn = isbn;
		this.author = author;
		this.title = title;
		this.description = description;
		if (Objects.isNull(category)) {
			this.category = new CategoryImpl(CategoryEnum.UNCLASSIFIED);
		} else {
			this.category = category;
		}
		if (Objects.isNull(keywords)) {
			this.keywords=new ArrayList<String>();
		} else{
			List<String> keyWordsList = new ArrayList<String>();
			for (String str :Arrays.asList(keywords)) {
				keyWordsList.add(str.toUpperCase());
			}
			this.keywords=new ArrayList<String>(keyWordsList);
		}
	}

	public BookImpl(Long isbn, String author, String title, String description,
			Category category, double price, String... keywords)
			throws RemoteException {
		this(isbn, author, title, description, category, keywords);
		if (price < 0) {
			throw new IllegalArgumentException("Price must be >=0");
		}
		this.price = price;

	}

	@Override
	public String getTitle() throws RemoteException {

		return title;
	}

	@Override
	public void setTitle(String title) throws RemoteException {
		this.title = title;

	}

	@Override
	public long getISBN() throws RemoteException {

		return isbn;
	}

	@Override
	public void setISBN(long isbn) throws RemoteException {
		this.isbn = isbn;

	}

	@Override
	public String getAutor() throws RemoteException {
		return author;
	}

	@Override
	public void setAuthor(String author) throws RemoteException {
		this.author = author;
	}

	@Override
	public String getDescription() throws RemoteException {
		return description;
	}

	@Override
	public void setDescription(String description) throws RemoteException {
		this.description = description;
	}

	@Override
	public String getCategory() throws RemoteException {
		return category.getCategory();
	}

	@Override
	public List<String> getKeyWords() throws RemoteException {
		return keywords;
	}

	@Override
	public String remoteToString() throws RemoteException {
		return new StringBuilder("ISBN  : ").append(isbn).append("\nTitle : ")
				.append(title).append("\nAutor : ").append(author)
				.append("\n Description : ").append(description)
				.append("\ncategory :").append(category).append("\n price : ")
				.append(price).toString();
	}

	@Override
	public double getPrice() throws RemoteException {
		return this.price;
	}

	@Override
	public void setPrice(double price) throws RemoteException {
		this.price = price;

	}

}
