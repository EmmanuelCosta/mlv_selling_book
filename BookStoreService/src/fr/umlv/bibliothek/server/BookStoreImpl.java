package fr.umlv.bibliothek.server;

import java.awt.RenderingHints.Key;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import fr.umlv.bibliothek.server.Category.CategoryEnum;

@SuppressWarnings("serial")
public class BookStoreImpl extends UnicastRemoteObject implements BookStore {

	private final Category category;
	private Map<Long, BookManager> storage = new HashMap<>();
	private Set<String> keyWordsSet = new HashSet<>();

	public BookStoreImpl(CategoryEnum category) throws RemoteException {
		this.category = new CategoryImpl(category);
	}

	public void createBook(Long isbn, String author, String title,
			String description, double price, int numberToAdd,
			String... keywords) throws RemoteException {

		BookManager bookManager = storage.get(isbn);
		if (Objects.isNull(bookManager)) {
			Book book = new BookImpl(isbn, author, title, description,
					this.category, price, keywords);
			BookManager newBookManager = new BookManager(book, numberToAdd);
			storage.put(isbn, newBookManager);

			List<String> keyWordsList = new ArrayList<String>();
			for (String str : book.getKeyWords()) {
				keyWordsList.add(str.toUpperCase());
			}
			this.keyWordsSet.addAll(keyWordsList);
		} else {
			bookManager.increaseAvailableBook(numberToAdd);
		}
	}

	@Override
	public void createBook(Long isbn, String author, String title,
			String description, double price, String... keywords)
			throws RemoteException {
		createBook(isbn, author, title, description, price, 1, keywords);
	}

	@Override
	public void addBook(Long isbn, int numberToAdd) throws RemoteException {
		BookManager bookManager = storage.get(isbn);
		if (Objects.isNull(bookManager)) {
			throw new IllegalArgumentException(
					"There is no Book With this isbn = " + isbn);
		} else {
			bookManager.increaseAvailableBook(numberToAdd);
		}

	}

	@Override
	public void addBook(Long isbn) throws RemoteException {
		addBook(isbn, 1);
	}

	@Override
	public Book removeBookByISBN(long isbn, int numberToRemove)
			throws RemoteException {

		BookManager bookManager = storage.get(isbn);
		if (Objects.isNull(bookManager)) {
			return null;
		}
		Book book = bookManager.getBook();
		boolean result = bookManager
				.decriseNumberOfAvailableBook(numberToRemove);
		if (result) {
			return book;
		} else {
			throw new IllegalArgumentException(
					"to decrease must be lest than available book.\n here there is available ="
							+ bookManager.getAvailable());
		}

	}

	@Override
	public Book removeBookByISBN(long isbn) throws RemoteException {
		return removeBookByISBN(isbn, 1);
	}

	@Override
	public Book getBookByISBN(long isbn) throws RemoteException {
		BookManager bookManager = storage.get(isbn);
		return (bookManager == null) ? null : bookManager.getBook();
	}

	@Override
	public List<Book> searchBookByAuthor(String author) throws RemoteException {

		List<Book> searchList = new ArrayList<>();
		for (BookManager bookManager : storage.values()) {
			Book book = bookManager.getBook();
			try {
				if (book.getAutor().equalsIgnoreCase(author)) {
					searchList.add(book);
				}
			} catch (Exception e) {
				System.out.println("search error " + e.getCause());
			}
		}
		return searchList;

	}

	@Override
	public List<Book> searchBookByKeyWords(String keyWord)
			throws RemoteException {
		List<Book> searchList = new ArrayList<>();
		String updateKeyWord = keyWord.toUpperCase();
		if (!this.keyWordsSet.contains(updateKeyWord)) {
			System.out.println("null");
			return searchList;
		}
		for (BookManager bookManager : storage.values()) {
			Book book = bookManager.getBook();
			try {
				if (book.getKeyWords().contains(updateKeyWord)) {
					searchList.add(book);
				}
			} catch (Exception e) {
				System.out.println("search error " + e.getCause());
			}
		}
		return searchList;
	}

	@Override
	public List<Book> searchBookByCategory(CategoryEnum category)
			throws RemoteException {
		List<Book> searchList = new ArrayList<>();
		for (BookManager bookManager : storage.values()) {
			Book book = bookManager.getBook();
			try {
				if (book.getCategory().equals(category.name())) {
					searchList.add(book);
				}
			} catch (Exception e) {
				System.out.println("search error " + e.getCause());
			}
		}
		return searchList;
	}

	@Override
	public List<Book> searchBookByTitle(String title) throws RemoteException {
		List<Book> searchList = new ArrayList<>();
		for (BookManager bookManager : storage.values()) {
			Book book = bookManager.getBook();
			try {
				if (book.getTitle().equalsIgnoreCase(title)) {
					searchList.add(book);
				}
			} catch (Exception e) {
				System.out.println("search error " + e.getCause());
			}
		}
		return searchList;
	}

	@Override
	public String getCategory() throws RemoteException {
		return category.getCategory();
	}

	@Override
	public List<Book> searchBookByPrice(double price) throws RemoteException {
		List<Book> searchList = new ArrayList<>();
		for (BookManager bookManager : storage.values()) {
			Book book = bookManager.getBook();
			try {
				if (book.getPrice() <= price) {
					searchList.add(book);
				}
			} catch (Exception e) {
				System.out.println("search error " + e);
			}
		}
		return searchList;
	}

	@Override
	public List<Book> getAllBooks() throws RemoteException {
		List<Book> bookList = new ArrayList<>();
		for (BookManager bookManager : storage.values()) {
			Book book = bookManager.getBook();
			bookList.add(book);
		}
		return bookList;
	}

	@Override
	public boolean isBookAvailable(Long isbn) throws RemoteException {
		BookManager bookManager = storage.get(isbn);
		return (bookManager == null) ? false : bookManager.isAvailable();
	}

	@Override
	public int getNumberOfAvailableBook(Long isbn) throws RemoteException {
		BookManager bookManager = storage.get(isbn);
		return (bookManager == null) ? 0 : bookManager.getAvailable();
	}

	@Override
	public void resetBookStore() throws RemoteException

	{
		this.storage = new HashMap<Long, BookManager>();
		this.keyWordsSet = new HashSet<>();

	}
}

