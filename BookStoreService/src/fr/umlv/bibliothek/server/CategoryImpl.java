package fr.umlv.bibliothek.server;

import java.io.Serializable;

public class CategoryImpl implements Serializable, Category {
	private final String category;

	public CategoryImpl(CategoryEnum category) {
		this.category = category.name();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.umlv.biblothek.common.Categories#getCategory()
	 */
	public String getCategory() {
		return category;
	}

	@Override
	public boolean equals(Object obj) {

		if (!(obj instanceof Category)) {
			return false;
		}
		Category cat = (Category) obj;
		return category.equals(cat.getCategory());

	}
}
