package fr.umlv.bibliothek.server;

public class BookManager {
	private Book book;
	private int available;

	public BookManager(Book book, int available) {
		this.book = book;
		this.available = available;
	}

	public Book getBook() {
		return book;
	}

	public int getAvailable() {
		return available;
	}

	public boolean decriseNumberOfAvailableBook(int toDecrease) {
		int number = this.available - toDecrease;
		if (number < 0) {
			return false;
		} else {
			this.available = number;
			return true;
		}
	}

	public void increaseAvailableBook(int toIncrease) {
		this.available += toIncrease;
	}

	public boolean isAvailable() {
		return this.available > 0;
	}

}
