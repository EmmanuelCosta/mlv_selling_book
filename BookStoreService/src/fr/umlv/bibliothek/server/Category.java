package fr.umlv.bibliothek.server;


public interface Category  {
	public enum CategoryEnum {
		PROG_OBJ, WEB, HACK, ELECTRO, PROG_FONC, PROG_IMPERATIVE, PROG_C, UNCLASSIFIED;
	}

	public String getCategory();
}